from wagtail.api.v2.views import BaseAPIViewSet
from wagtail.api.v2.filters import FieldsFilter
from show.models import Show
from slot.models import Slot

class ShowAPIEndpoint(BaseAPIViewSet):
    ...
    model = Show
    BaseAPIViewSet.filter_backends.append(FieldsFilter)


class SlotAPIEndpoint(BaseAPIViewSet):
    ...
    model = Slot
    BaseAPIViewSet.filter_backends.append(FieldsFilter)