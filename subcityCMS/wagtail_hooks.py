from django.utils.html import format_html
from django.templatetags.static import static
from django.urls import reverse

from wagtail.admin.menu import MenuItem
from wagtail import hooks

@hooks.register('insert_global_admin_css')
def global_admin_css():
    return format_html('<link rel="stylesheet" href="{}">', static('css/subcityCMS.css'))

@hooks.register('register_admin_menu_item')
def register_frontend_menu_item():
  return MenuItem('Frontend', reverse('index'), icon_name='desktop', order=1)

@hooks.register('construct_main_menu')
def hide_things_from_contributors(request, menu_items):
  if not (request.user.groups.filter(name='Team').exists() or request.user.is_superuser):
    menu_items[:] = [item for item in menu_items if (item.name != 'comms' and item.name != 'support')]