from django.conf import settings
from django.urls import include, path, re_path
from django.contrib import admin

from wagtail.admin import urls as wagtailadmin_urls
from wagtail import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls
from wagtail.images.views.serve import ServeView


from schedule.views import frontendSchedule, nowPlaying, showFromTimestamp
from subcityAPI.views import desiredRecordings

from .api import api_router

urlpatterns = [
    path("", include("frontend.urls")),  
    path('django-admin/', admin.site.urls),

    path('admin/', include(wagtailadmin_urls)),
    path('documents/', include(wagtaildocs_urls)),
    path('api/schedule/<urlDate>/', frontendSchedule, name='frontendSchedule'),
    path('api/schedule/', frontendSchedule, name='frontendSchedule'),
    path('api/nowplaying/', nowPlaying, name="nowPlaying"),
    path('api/showFromTimestamp/<yyyy_mm_dd_hh_mm_ss>/', showFromTimestamp, name="showFromTimestamp"),
    path('api/desiredRecordings/', desiredRecordings, name="desiredRecordings"),
    re_path(r'^images/([^/]*)/(\d*)/([^/]*)/[^/]*$', ServeView.as_view(), name='wagtailimages_serve'),
    # ...
    # This line MUST come before the bottom url pattern
    path('api/v2/', api_router.urls),

]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = urlpatterns + [
    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:

    path("", include(wagtail_urls)),

    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    path("pages/", include(wagtail_urls)),
]
