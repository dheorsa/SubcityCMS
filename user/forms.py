from django import forms
from django.utils.translation import gettext_lazy as _
import django

from wagtail.users.forms import UserEditForm, UserCreationForm

class CustomUserEditForm(UserEditForm):
    pronouns = forms.CharField(required=False, label=_("Pronouns"))
    bio = forms.CharField(required=False, label=_("Bio"), widget=forms.Textarea)


class CustomUserCreationForm(UserCreationForm):
    pronouns = forms.CharField(required=False, label=_("Pronouns"))
    bio = forms.CharField(required=False, label=_("Bio"), widget=forms.Textarea)

class CustomUserSettingsForm(forms.ModelForm):
    pronouns = forms.CharField(required=False, label=_("Pronouns"))
    bio = forms.CharField(required=False, label=_("Bio"), widget=forms.Textarea)
    class Meta:
        model = django.contrib.auth.get_user_model()
        fields = ['pronouns', 'bio']