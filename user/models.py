from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.db.models.functions import Lower

class SubcityUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().order_by(Lower('first_name'), Lower('last_name'))

class User(AbstractUser):
    id = models.AutoField(primary_key=True)
    bio = models.CharField(blank=True, null=True, max_length=2048)
    pronouns = models.CharField(blank=True, null=True, max_length=64)
    class Meta:
        ordering = ['username']

    def __str__(self):
        if self.first_name and self.last_name:
            return self.first_name + " " + self.last_name
        else:
            return self.username
        
    objects = SubcityUserManager()

