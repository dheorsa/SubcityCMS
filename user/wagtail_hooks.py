from wagtail.admin.views.account import BaseSettingsPanel
from wagtail import hooks
from .forms import CustomUserSettingsForm

@hooks.register('register_account_settings_panel')
class CustomSettingsPanel(BaseSettingsPanel):
    name = 'about'
    title = "About Me"
    order = 300
    form_class = CustomUserSettingsForm
    form_object = 'user'
