from wagtail_modeladmin.options import ModelAdmin, modeladmin_register
from .models import Show
from wagtail_modeladmin.helpers import PermissionHelper
from django.contrib.admin import SimpleListFilter
from wagtail_modeladmin.mixins import ThumbnailMixin

class ShowPermissionHelper(PermissionHelper):
    def user_can_edit_obj(self, user, obj):
        """
        Return a boolean to indicate whether `user` is permitted to 'change'
        a specific `self.model` instance.
        """

        if (user.is_superuser or user.groups.filter(name='Team').exists() or user in obj.contributors.all()):
            perm_codename = self.get_perm_codename('change')
            return self.user_has_specific_permission(user, perm_codename)
            # The above two lines are just what the default PermissionHelper does
        else:
            return False

class ActiveFilter(SimpleListFilter):
    title = "whether show is active"
    parameter_name = "active"

    def lookups(self, request, model_admin):
        return [('yes',"Yes"), ('no', "No")]

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(active=True)
        elif self.value() == 'no':
            return queryset.filter(active=False)

class AutoGenerateFilter(SimpleListFilter):
    title = "whether show has a regular slot"
    parameter_name = "auto_generate_slots"

    def lookups(self, request, model_admin):
        return [('yes',"Yes"), ('no', "No")]

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(auto_generate_slots=True)
        elif self.value() == 'no':
            return queryset.filter(auto_generate_slots=False)

class ShowAdmin(ThumbnailMixin, ModelAdmin):
    """Show Admin"""

    def format_contributors_for_list_view(self, obj):
        x = ""
        for u in obj.contributors.all()[:]:
            x += (", " + str(u))
        return(x[2:])
    format_contributors_for_list_view.short_description = 'contributors'

    model = Show
    menu_label = 'Shows'  # ditch this to use verbose_name_plural from model
    menu_icon = 'media'  # see the styleguide for more options
    menu_order = 000  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('admin_thumb', 'name', 'description', 'format_contributors_for_list_view', 'slug',)
    list_display_add_buttons = 'name'
    search_fields = ('name', 'description', 'genres__name')
    base_url_path = "shows"
    permission_helper_class = ShowPermissionHelper
    list_filter = (ActiveFilter, AutoGenerateFilter, 'repeat_frequency',)
    thumb_image_field_name = 'image'


    # Uncomment this to only display the shows a contributor owns to them in the list.
    # With it commented, they still can't edit other shows but they can read their info
    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:
    #         return qs
    #     elif request.user.groups.filter(name='Contributors').exists():
    #         return qs.filter(contributor=request.user)
    #     else:
    #         return None

modeladmin_register(ShowAdmin)
