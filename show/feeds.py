from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Rss201rev2Feed
from .models import Show
from slot.models import Slot
from wagtail.images.views.serve import generate_image_url
from django.http import Http404

class SubcityFeedGenerator(Rss201rev2Feed):
    def rss_attributes(self):
        return {u"version": self._version, u"xmlns:atom": u"http://www.w3.org/2005/Atom", u'xmlns:itunes': u'http://www.itunes.com/dtds/podcast-1.0.dtd'}

    def add_root_elements(self, handler):
        super(SubcityFeedGenerator, self).add_root_elements(handler)
        handler.addQuickElement(f'itunes:image href="{self.feed["iTunes_image_url"]}"')
    
    def add_item_elements(self, handler, item):
        super(SubcityFeedGenerator, self).add_item_elements(handler, item)
        if "iTunes_image_url" in item:
            handler.addQuickElement(f'itunes:image href="{item["iTunes_image_url"]}"')
    


class ShowFeed(Feed):
    feed_type = SubcityFeedGenerator

    def get_object(self, request, id):
        if id is None:
            raise Http404("Show not found")
        if id.isnumeric():
            if Show.objects.filter(id=id).exists():
                return Show.objects.get(id=id)
            else:
                raise Http404("Show not found")
        elif Show.objects.filter(slug=id).exists():
            return Show.objects.get(slug=id)
        else:
            raise Http404("Show not found")

    def title(self, obj):
        return obj.name
    
    def description(self, obj):
        return obj.description

    def link(self, obj):
        return "https://subcity.org/show/" + str(obj.id)

    def items(self, obj):
        return Slot.objects.filter(show__id=obj.id, publish_recording=True)
    
    def item_link(self, item):
        return "https://subcity.org" + item.getLink()
    
    def item_enclosure_url(self, item):
        return item.recording_url

    def item_enclosure_length(self, item):
        return int((item.end_time - item.start_time).total_seconds())
    
    item_enclosure_mime_type = "audio/mpeg"

    def item_pubdate(self, item):
        return item.start_time

    item_copyright = ""

    def item_title(self, item):
        if item.episode_title:
            return f"{item.show.name}: {item.episode_title} {item.start_time:%-d/%-m/%Y}"
        else:
            return f"{item.show.name} {item.start_time:%-d/%-m/%Y}"
    
    def item_description(self, item):
        if item.episode_description:
            return item.episode_description
        else:
            return str(item)

    def feed_guid(self, obj):
        return obj.id
    
    def item_guid(self, item):
        return item.id
    
    item_guid_is_permalink = False

    def feed_extra_kwargs(self, obj):
        extra = {}
        if obj.image:
            iTunes_image_url = "https://subcity.org" + generate_image_url(obj.image, 'fill-1400x1400')
        else:
            iTunes_image_url = "https://subcity.org/static/images/subcity_logo_square.jpg"

        extra['iTunes_image_url'] = iTunes_image_url
        return extra

    def item_extra_kwargs(self, item):
        extra = {}
        if item.episode_image:
            extra['iTunes_image_url'] = "https://subcity.org" + generate_image_url(item.episode_image, 'fill-1400x1400')

        return extra
    