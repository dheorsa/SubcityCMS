from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django import forms
from wagtail.api import APIField
from wagtail.admin.panels import FieldPanel, FieldRowPanel, MultiFieldPanel
from datetime import timedelta, datetime, date
from taggit.models import TaggedItemBase
from modelcluster.fields import ForeignKey
from modelcluster.contrib.taggit import TaggableManager
from django.apps import apps
from django.db.models.functions import Lower

class ShowManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by(Lower('name'))

DAYS_AHEAD_TO_GENERATE = 7 * 8 #autogenerate slots 8 weeks in advance
def doesItClash(newSlot):
    return apps.get_model("slot", "Slot").objects.filter(end_time__gt=newSlot.start_time, start_time__lt=newSlot.end_time).exists()

class Genre(TaggedItemBase):
    id = models.AutoField(primary_key=True)
    content_object = ForeignKey('show.Show', on_delete=models.CASCADE, related_name='tagged_items')

class Show(models.Model):
    """Django model representation of a show"""
    class Meta:
        permissions = [
            ("fully_edit_show", "Can fully edit"),
        ]
        ordering = ['name']

    id = models.AutoField(primary_key=True)
    slug = models.CharField(unique=True, blank=False, null=False, max_length=32, help_text="A unique identifier, characters [a-z] and [0-9], used to create a custom show URL.")
    name = models.CharField(blank=False, null=False, unique=True, max_length=256)
    description = models.CharField(blank=True, null=True, max_length=4096)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True, blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    genres = TaggableManager(through=Genre, blank=True, help_text="")

    active = models.BooleanField(blank=False, null=False, default=True, help_text="Do we expect there to be more episodes of this show in the future?")

    send_reminders = models.BooleanField(blank=False, null=False, default=False, help_text="Remind the contributor(s) via email each time their show is in the schedule, two days in a advance")

    contributors = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=False, help_text="Who runs this show?")

    auto_generate_slots = models.BooleanField(blank=False, null=False, default=False, help_text="Should this show be automatically placed in the schedule?")
    __previous_autogen = None #used to check if autogenerate status has changed
    regular_start_time = models.TimeField(blank=True, null=True, auto_now=False, auto_now_add=False, help_text="The start time for auto-generated slots")
    regular_end_time = models.TimeField(blank=True, null=True, auto_now=False, auto_now_add=False, help_text="The end time for auto-generated slots")
    
    first_day_of_regular_slot = models.DateField(blank=True, null=True, auto_now=False, auto_now_add=False, help_text="The day of the first auto-generated slot")
    __previous_first_day = None #used to check if first day of regular slot has changed

    REPEAT_FREQUENCIES = [
        ('1', 'Every Week'),
        ('2', 'Every 2 Weeks'),
        ('4', 'Every 4 Weeks'),
        ('8', 'Every 8 Weeks'),
    ]
    repeat_frequency = models.CharField(blank=True, null=True, max_length=1, choices=REPEAT_FREQUENCIES, help_text="How frequent the regular slot is. Every 2 weeks means one week on, one week off. Every 4 is our equivalent of \"monthly\" (we arrange slots based on weeks, not calendar months)")

    mixcloud_listen_back_link = models.URLField(blank=True, null=True)

    def getLink(self):
        if self.slug:
            return "/show/" + self.slug
        else:
            return "/show/" + str(self.id)

    # export fields over the api
    api_fields = [
        APIField('id'),
        APIField('slug'),
        APIField('name'),
        APIField('description'),
        APIField('image'),
        APIField('active'),
    ]

    panels = [
        MultiFieldPanel([
            FieldPanel('name', permission="show.fully_edit_show"),
            FieldPanel('slug', permission="show.fully_edit_show"),
            FieldPanel('description', widget=forms.Textarea),
            FieldPanel('image'),
            FieldPanel('genres', heading="Genres"),
            FieldPanel('active', permission="show.fully_edit_show"),
            FieldRowPanel([
                FieldPanel('contributors', permission="show.fully_edit_show"),
                FieldPanel('send_reminders'),
            ]),
        ], heading="Show Details"),
        MultiFieldPanel([
            FieldPanel('auto_generate_slots', permission="show.fully_edit_show"),
            FieldRowPanel([
                FieldPanel('regular_start_time', permission="show.fully_edit_show"),
                FieldPanel('regular_end_time', permission="show.fully_edit_show"),
            ]),
            FieldRowPanel([
                FieldPanel('first_day_of_regular_slot', permission="show.fully_edit_show"),
                FieldPanel('repeat_frequency', permission="show.fully_edit_show"),
            ]),
        ], heading="Regular Slot"),
        MultiFieldPanel([
            FieldPanel('mixcloud_listen_back_link', permission="show.fully_edit_show", heading="Mixcloud Playlist (Listen Back)")
        ], heading="Links")
    ]

    def __init__(self, *args, **kwargs):
        super(Show, self).__init__(*args, **kwargs)
        self.__previous_autogen = self.auto_generate_slots
        self.__previous_first_day = self.first_day_of_regular_slot

    def clean(self):
        if self.slug:
            if self.slug.isnumeric():
                raise ValidationError(
                        {'slug': "Slug must contain at least one letter"}
                    )
            for c in self.slug:
                if c not in "abcdefghijklmnopqrstuvwxyz1234567890":
                    raise ValidationError(
                        {'slug': "Slug must only contain lowercase letters and numbers"}
                    )

        if self.auto_generate_slots == True:
            if self.regular_start_time == None:
                raise ValidationError(
                    {'regular_start_time': "You need a start time to auto-generate calendar slots"}
                )
            if self.regular_end_time == None:
                raise ValidationError(
                    {'regular_end_time': "You need an end time to auto-generate calendar slots"}
                )
            if self.first_day_of_regular_slot == None:
                raise ValidationError(
                    {'first_day_of_regular_slot': "This is required to auto-generate calendar slots"}
                )
            if self.repeat_frequency == None:
                raise ValidationError(
                    {'repeat_frequency': "This is required to auto-generate calendar slots"}
                )
            if self.active == False:
                raise ValidationError(
                    {'auto_generate_slots': "Inactive shows shouldn't automatically generate new slots"}
                )

            #if first day has been changed
            if self.__previous_first_day != self.first_day_of_regular_slot:
                if self.first_day_of_regular_slot < date.today():
                    raise ValidationError(
                        {'first_day_of_regular_slot': "Please pick a day that isn't in the past"}
                    )
                
            #kinda complicated but just checking if any other regular slots clash with the new regular slot
            #this could probably do with refactoring
            possible_clashes = apps.get_model("show", "Show").objects.filter(auto_generate_slots = True, regular_end_time__gt=self.regular_start_time, regular_start_time__lt=self.regular_end_time, first_day_of_regular_slot__week_day=[1,2,3,4,5,6,7,1][self.first_day_of_regular_slot.isoweekday()]).exclude(id=self.id)
            for show in possible_clashes:
                day = show.first_day_of_regular_slot

                while day <= self.first_day_of_regular_slot:
                    if day == self.first_day_of_regular_slot:
                        raise ValidationError(
                            {'auto_generate_slots': "This clashes with the show " + str(show) + ", which happens from " + show.regular_start_time.strftime("%H:%M") + " to " + show.regular_end_time.strftime("%H:%M")}
                        )
                    day += timedelta(days = (7 * int(self.repeat_frequency)))

        if self.regular_start_time is not None and self.regular_end_time is not None:
            if self.regular_end_time < self.regular_start_time:
                raise ValidationError(
                    {'regular_end_time': "End time must come after start time"}
                )
            if datetime.combine(date.today(), self.regular_end_time) < (datetime.combine(date.today(), self.regular_start_time) + timedelta(minutes=15)):
                raise ValidationError(
                    {'regular_end_time': "Slots must be at least 15 minutes long"}
                )
        
        
    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        generateSlots = False
        if self.auto_generate_slots == True and self.__previous_autogen == False:
            # if autogeneration has been switched on
            generateSlots = True
            
        super(Show, self).save(force_insert, force_update, *args, **kwargs)

        if generateSlots:
            day = self.first_day_of_regular_slot
            endDay = date.today() + timedelta(days=DAYS_AHEAD_TO_GENERATE)
            while day <= endDay:
                s = apps.get_model("slot", "Slot")(show=self, slot_type=apps.get_model("slot", "Slot").EPISODE, start_time=datetime.combine(day, self.regular_start_time), end_time=datetime.combine(day, self.regular_end_time))
                if not doesItClash(s):
                    s.save()
                day += timedelta(days=(7 * int(self.repeat_frequency)))

        self.__previous_autogen = self.auto_generate_slots
        self.__previous_first_day = self.first_day_of_regular_slot

    def __str__(self):
        """String representation of this show"""
        return self.name
    
    objects = ShowManager()
