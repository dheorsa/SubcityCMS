# Generated by Django 4.2.13 on 2024-05-25 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('show', '0012_alter_show_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='show',
            name='description',
            field=models.CharField(blank=True, max_length=4096, null=True),
        ),
    ]
