
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "subcityCMS.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from wagtail.images.models import Image
from wagtail.models import Page, Site
from wagtail.models import Collection

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission
from wagtail.models import GroupCollectionPermission, GroupPagePermission
import datetime as dt

from show.models import Show
from slot.models import Slot
from user.models import User
from teamDay.models import TeamDay

from frontend.models import HomePage, ContentPage

# For getting permission codenames from a group you've assigned permissions to manually, use
#for permission in Group.objects.filter(name="Contributors")[0].permissions.all():
    #print(permission.codename)
# So you can then put those permissions in this script

def populate():

    # These groups are auto generated and we don't want them
    Group.objects.filter(name="Editors").delete()
    Group.objects.filter(name="Moderators").delete()

    root_collection = Collection.get_first_root_node()

    if not Collection.objects.filter(name="Promo").exists():
        print("Creating Promo Collections")
        root_collection.add_child(name="Promo")
        Collection.objects.get(name="Promo").add_child(name="Show Images")
        Collection.objects.get(name="Promo").add_child(name="Episode Images")
        Collection.objects.get(name="Promo").add_child(name="Booth Pics")

    if not Collection.objects.filter(name="Recordings").exists():
        print("Creating Recordings Collection")
        root_collection.add_child(name="Recordings")

    user_ct = ContentType.objects.get_for_model(User)
    show_ct = ContentType.objects.get_for_model(Show)
    slot_ct = ContentType.objects.get_for_model(Slot)
    admin_ct = ContentType.objects.get(app_label="wagtailadmin")
    team_day_ct = ContentType.objects.get_for_model(TeamDay)

    team_group, created = Group.objects.get_or_create(name='Team')
    if created:
        print("Created Team Group")
        team_group.permissions.add(Permission.objects.get(content_type=user_ct,codename='add_user'))
        team_group.permissions.add(Permission.objects.get(content_type=user_ct,codename='change_user'))
        team_group.permissions.add(Permission.objects.get(content_type=user_ct,codename='delete_user'))

        team_group.permissions.add(Permission.objects.get(content_type=show_ct,codename='add_show'))
        team_group.permissions.add(Permission.objects.get(content_type=show_ct,codename='change_show'))
        team_group.permissions.add(Permission.objects.get(content_type=show_ct,codename='delete_show'))
        team_group.permissions.add(Permission.objects.get(content_type=show_ct,codename='fully_edit_show'))

        team_group.permissions.add(Permission.objects.get(content_type=slot_ct,codename='add_slot'))
        team_group.permissions.add(Permission.objects.get(content_type=slot_ct,codename='change_slot'))
        team_group.permissions.add(Permission.objects.get(content_type=slot_ct,codename='delete_slot'))
        team_group.permissions.add(Permission.objects.get(content_type=slot_ct,codename='fully_edit_slot'))

        team_group.permissions.add(Permission.objects.get(content_type=admin_ct,codename='access_admin'))

        team_group.permissions.add(Permission.objects.get(content_type=team_day_ct,codename='add_teamday'))
        team_group.permissions.add(Permission.objects.get(content_type=team_day_ct,codename='change_teamday'))
        team_group.permissions.add(Permission.objects.get(content_type=team_day_ct,codename='delete_teamday'))

        GroupCollectionPermission.objects.create(group=team_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtailimages', codename='add_image'))
        GroupCollectionPermission.objects.create(group=team_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtailimages', codename='change_image'))
        GroupCollectionPermission.objects.create(group=team_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtailimages', codename='choose_image'))

        GroupCollectionPermission.objects.create(group=team_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtaildocs', codename='add_document'))
        GroupCollectionPermission.objects.create(group=team_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtaildocs', codename='change_document'))
        GroupCollectionPermission.objects.create(group=team_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtaildocs', codename='choose_document'))


    contributors_group, created = Group.objects.get_or_create(name='Contributors')
    if created:
        print("Created Contributors Group")
        #contributors_group.permissions.add(Permission.objects.get(content_type=show_ct,codename='change_show'))

        #contributors_group.permissions.add(Permission.objects.get(content_type=slot_ct,codename='change_slot'))

        #contributors_group.permissions.add(Permission.objects.get(content_type=admin_ct,codename='access_admin'))

        #GroupCollectionPermission.objects.create(group=contributors_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtailimages', codename='add_image'))
        #GroupCollectionPermission.objects.create(group=contributors_group, collection=root_collection, permission=Permission.objects.get(content_type__app_label='wagtailimages', codename='choose_image'))

    # If this is the first time the script is run on the new install, we need to create the page structure.
    if Page.objects.filter(id=2).exists():
        Page.objects.filter(id=2).delete()
        root_page = Page.objects.get(id=1)
        home_page = HomePage(title="Subcity Radio")
        root_page.add_child(instance=home_page)
        home_page.add_child(instance=ContentPage(title="Archive", body="This is the archives page. You can change this text in the backend"))
        home_page.add_child(instance=ContentPage(title="About", body="This is the about page. You can change this text in the backend"))
        home_page.add_child(instance=ContentPage(title="Events", body="This is the events page. You can change this text in the backend"))
        home_page.add_child(instance=ContentPage(title="Get Involved", body="This is the get involved page. You can change this text in the backend"))
        home_page.add_child(instance=ContentPage(title="Blog", body="This is the blog page. You can change this text in the backend"))
        GroupPagePermission.objects.create(group=team_group, page=root_page, permission=Permission.objects.get(content_type__app_label="wagtailcore", codename="add_page"))
        GroupPagePermission.objects.create(group=team_group, page=root_page, permission=Permission.objects.get(content_type__app_label="wagtailcore", codename="change_page"))
        GroupPagePermission.objects.create(group=team_group, page=root_page, permission=Permission.objects.get(content_type__app_label="wagtailcore", codename="publish_page"))
        Site.objects.create(hostname='localhost', root_page=home_page, is_default_site=True)
        print("Created Pages")

# Start execution here!
if __name__ == '__main__':
    print("Starting CMS population script...")
    populate()