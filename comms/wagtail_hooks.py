from django.urls import path, reverse
from wagtail import hooks
from wagtail.admin.menu import MenuItem

from .views import index, insta_story


@hooks.register('register_admin_urls')
def register_comms_url():
    return [
        path('comms/', index, name='comms'),
        path("comms/insta_story/<slide_number>/", insta_story, name='insta_story'),
        path('comms/insta_story/', insta_story, name='insta_story'),
    ]


@hooks.register('register_admin_menu_item')
def register_comms_menu_item():
    return MenuItem('Comms', reverse('comms'), icon_name='comment')