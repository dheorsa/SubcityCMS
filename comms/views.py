from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseNotFound, FileResponse, HttpResponse
import datetime
import time
from slot.models import Slot
from django.contrib.staticfiles.finders import find as find_static_file
from PIL import Image, ImageDraw, ImageFont
import random
from django.urls import reverse

def slot_time_to_string(timeobj):
    if timeobj.minute == 0:
        return str(timeobj.hour).zfill(2)
    else:
        return str(timeobj.hour).zfill(2) + ":" + str(timeobj.minute).zfill(2)
    
def slot_to_string(slotobj):
    if slotobj.episode_title:
        return str(slotobj.show) + " - " + slotobj.episode_title
    else:
        return str(slotobj.show)
    
def compute_line_width(text, font):
    d = ImageDraw.Draw(Image.new("RGBA", (1,1), (255,255,255)))
    return d.textlength(text, font=font)





def wrap_text(slots, font):
    #returns a list [{'page':page,'x':x,'y':y,'text':text, 'align':alignment}] of text objects to be created
    output = []
    page_count = 0
    line_count = 0
    entry_count = 0
    for slot in slots:
        top_line = line_count
        lines = [""]
        max_line_width = 630

        slot_words = []

        for word in slot_to_string(slot).split():
            if compute_line_width(word, font) > max_line_width:
                slot_words += [(word[:len(word)//2])+"-", word[len(word)//2:]] #split word in half and add "- " to first half
            else:
                slot_words.append(word)

        for word in slot_words:

            if compute_line_width(lines[-1], font) + compute_line_width(" " + word, font) < max_line_width: #if next word would fit on line
                if lines[-1] == "":
                    lines[-1] += word #start line with word
                else:
                    lines[-1] += (" " + word) #continue line with space plus word

            else:
                lines.append(word) #start a new line
                line_count += 1

                if ((line_count*70) + (entry_count*35) + 70) > 840: #if we've overflowed page
                    page_count += 1
                    line_count = line_count - top_line
                    entry_count = 0
                    top_line = 0
                
        for c, line in enumerate(lines):
            output.append({'page':page_count, 'x':370, 'y':310+(70*(top_line + c))+(35*entry_count), 'text':line, 'anchor':'la'})


        output.append({'page':page_count, 'x':170, 'y':310+(70*top_line)+(35*entry_count), 'text':slot_time_to_string(slot.start_time), 'anchor':'ra'}) #add start text
        output.append({'page':page_count, 'x':195, 'y':310+(70*top_line)+(35*entry_count), 'text':"-", 'anchor':'ma'}) #add the - separator
        output.append({'page':page_count, 'x':220, 'y':310+(70*top_line)+(35*entry_count), 'text':slot_time_to_string(slot.end_time), 'anchor':'la'}) #add end text
        entry_count += 1
        line_count += 1

    return output


def get_insta_stories(slots):

    text_font = ImageFont.truetype(font=find_static_file("fonts/OpenSauceSans-Regular.ttf"), size=56)
    title_font = ImageFont.truetype(font=find_static_file("fonts/OpenSauceSans-Regular.ttf"), size=85)

    pages={}

    text_objects = wrap_text(slots, text_font)
    random.seed(int(time.mktime(datetime.date.today().timetuple())))
    background_numbers = random.sample(range(1,8), 7)
    print(background_numbers)

    for i, e in enumerate(text_objects):
        if e['page'] not in pages:
            pages[e['page']] = Image.open(find_static_file(f"images/story{background_numbers[i % len(background_numbers)]}.png"))
            ImageDraw.Draw(pages[e['page']]).text(xy=(80,130), text=datetime.datetime.today().strftime('%A'), fill=(255,255,255), font=title_font, anchor="la")

        ImageDraw.Draw(pages[e['page']]).text(xy=(e['x'],e['y']), text=e['text'], fill=(255,255,255), font=text_font, anchor=e['anchor'])
    return pages

def get_insta_story_urls(slots):
    stories = get_insta_stories(slots)
    urls = []
    for k in stories:
        urls.append(reverse('insta_story') + str(k))
    print("urls", urls)
    return urls
                

def insta_story(request, slide_number=0):
    if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:
        slide_number = int(slide_number)

        slots = Slot.objects.filter(
            start_time__date = datetime.date.today(),
            slot_type = Slot.EPISODE
        ).order_by('start_time')

        stories = get_insta_stories(slots)

        if slide_number in stories:
            response = HttpResponse(content_type='image/png')
            stories[slide_number].save(response, 'PNG')
            return response
        else:
            return HttpResponseNotFound("Out of Range")

    else:
        raise PermissionDenied

def index(request):
    if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:
        slots = Slot.objects.filter(
            start_time__date = datetime.date.today(),
            slot_type = Slot.EPISODE
        ).order_by('start_time')

        episodes_with_comms = []
        for slot in slots:
            if slot.episode_title or slot.episode_description or slot.episode_image:
                if slot.episode_title:
                    title = f"{slot.start_time.strftime('%H:%M')} | {slot.show.name} - {slot.episode_title}"
                else:
                    title = f"{slot.start_time.strftime('%H:%M')} | {slot.show.name}"

                if slot.episode_description:
                    description = slot.episode_description
                else:
                    description = slot.show.description

                if slot.episode_image:
                    image = slot.episode_image
                else:
                    image = slot.show.image                
                episodes_with_comms.append({"title":title,"description":description,"image":image})

        return render(request, "comms/index.html", {
            'story_urls':get_insta_story_urls(slots),
            'episodes_with_comms': episodes_with_comms,
            })
    else:
        raise PermissionDenied
