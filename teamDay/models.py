from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError

class TeamDay(models.Model):
    id = models.AutoField(primary_key=True)

    team_member = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, on_delete=models.PROTECT)

    day = models.DateField(blank=False, null=False)

    TEAMS = [
        ('p', 'Programmes'),
        ('c', 'Comms'),
    ]

    team = models.CharField(blank=False, null=False, max_length=1, choices=TEAMS, help_text="Programmes day or comms day?")

    def clean(self):
        if not self.team_member.groups.filter(name='Team').exists():
            raise ValidationError(
                {'team_member': "This user isn't on the team"}
            )

    class Meta:
        unique_together = ('team', 'day',)

    def __str__(self):
        if self.team == 'p':
            return str(self.team_member.first_name) + "'s programmes day on " + str(self.day)
        elif self.team == 'c':
            return str(self.team_member.first_name) + "'s comms day on " + str(self.day)

    
