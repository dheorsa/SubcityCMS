from wagtail_modeladmin.options import ModelAdmin, modeladmin_register
from .models import TeamDay
from wagtail_modeladmin.views import CreateView, EditView, DeleteView
from dateutil import parser
from datetime import date
from django.urls import reverse
from django.db import models, transaction
from wagtail.log_actions import log
from wagtail.admin import messages
from django.shortcuts import redirect
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.fields.related import ManyToManyField, OneToOneRel
from django.utils.translation import gettext as _

class TeamDayCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        self.requestUser = request.user
        self.requestTeam = request.GET.get('team', None)
        try:
            self.requestDate = parser.parse(request.GET.get('date', None)).date()
        except:
            self.requestDate = date.today()
        return super().get(self, request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        self.requestDate = parser.parse(form.data['day']).date()
        self.requestTeam = form.data['team']
        return super().post(self, request, *args, **kwargs)


    def get_instance(self):
        instance = super().get_instance()
        if hasattr(self, 'requestUser'):
            instance.team_member = self.requestUser
            instance.team = self.requestTeam
            instance.day = self.requestDate
        return instance
    
    def get_success_url(self):
        return reverse('schedule') + self.requestDate.strftime("%Y%m%d")
        
class TeamDayEditView(EditView):
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        self.requestDate = parser.parse(form.data['day']).date()
        self.requestTeam = form.data['team']
        return super().post(self, request, *args, **kwargs)
    
    def get_success_url(self):
        return reverse('schedule') + self.requestDate.strftime("%Y%m%d")
        
class TeamDayDeleteView(DeleteView):
    #this feels messy, try to find a way to decorate the inherited method rather than copy & tweak
    def post(self, request, *args, **kwargs):
        try:
            msg = _("%(model_name)s '%(object)s' deleted.") % {
                "model_name": self.verbose_name,
                "object": self.instance,
            }
            with transaction.atomic():
                log(instance=self.instance, action="wagtail.delete")
                self.delete_instance()
            messages.success(request, msg)
            return redirect(reverse('schedule') + self.instance.day.strftime("%Y%m%d"))
        except models.ProtectedError:
            linked_objects = []
            fields = self.model._meta.fields_map.values()
            fields = (
                obj for obj in fields if not isinstance(obj.field, ManyToManyField)
            )
            for rel in fields:
                if rel.on_delete == models.PROTECT:
                    if isinstance(rel, OneToOneRel):
                        try:
                            obj = getattr(self.instance, rel.get_accessor_name())
                        except ObjectDoesNotExist:
                            pass
                        else:
                            linked_objects.append(obj)
                    else:
                        qs = getattr(self.instance, rel.get_accessor_name())
                        for obj in qs.all():
                            linked_objects.append(obj)
            context = self.get_context_data(
                protected_error=True, linked_objects=linked_objects
            )
            return self.render_to_response(context)

    
class TeamDayAdmin(ModelAdmin):
    """Team Day Admin"""
    model = TeamDay
    base_url_path = "team_days"
    add_to_settings_menu = False
    add_to_admin_menu = False
    create_view_class = TeamDayCreateView
    edit_view_class = TeamDayEditView
    delete_view_class = TeamDayDeleteView
    menu_icon = 'date'


modeladmin_register(TeamDayAdmin)
