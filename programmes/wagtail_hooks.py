from django.urls import path, reverse
from wagtail import hooks
from wagtail.admin.menu import MenuItem

from .views import index


@hooks.register('register_admin_urls')
def register_programmes_url():
    return [
        path('programmes/', index, name='programmes'),
    ]


@hooks.register('register_admin_menu_item')
def register_programmes_menu_item():
    return MenuItem('Programmes', reverse('programmes'), icon_name='table')