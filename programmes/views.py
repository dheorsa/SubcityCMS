from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from datetime import datetime
from slot.models import Slot

def index(request):
    if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:
        return render(request, "programmes/index.html", {
            'pending_recordings': Slot.objects.filter(end_time__lt = datetime.now(), slot_type = Slot.EPISODE).exclude(recording_url__isnull=True).exclude(recording_url__exact='').exclude(publish_recording=True),
            })
    else:
        raise PermissionDenied
