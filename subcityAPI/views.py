from django.http import HttpResponse
from django.db.models import Q
import json
from slot.models import Slot
import datetime
from django.http import HttpResponse


def desiredRecordings(request):
    slots = Slot.objects.filter(end_time__lt=datetime.datetime.now() - datetime.timedelta(minutes=20),
                                end_time__gt=datetime.datetime.now() - datetime.timedelta(days=14),
                                slot_type=Slot.EPISODE).filter(Q(recording_url__isnull=True) | Q(recording_url=""))

    output = []

    for s in slots:
        if s.show.slug:
            output.append({"filename":s.show.slug + s.start_time.strftime("-%Y%m%d%H%M.mp3"), "start_time":(s.start_time - datetime.timedelta(minutes=5)).strftime("%Y/%m/%d/%H_%M_%S"), "end_time":(s.end_time + datetime.timedelta(minutes=15)).strftime("%Y/%m/%d/%H_%M_%S")})

    return HttpResponse(json.dumps(output))

