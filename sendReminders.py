import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "subcityCMS.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from slot.models import Slot
from show.models import Show
from datetime import date, time, timedelta, datetime
from django.core.mail import send_mail
from teamDay.models import TeamDay
from teamDay.admin import TeamDayAdmin

# run each day, look at every slot the day after tomorrow

dayAfterTomorrow = date.today() + timedelta(days = 2)

slots = Slot.objects.filter(start_time__date = dayAfterTomorrow).filter(show__send_reminders = True).filter(slot_type=Slot.EPISODE)

for slot in slots:
    send_mail(
        subject=f"Reminder: Subcity Radio show on {slot.start_time.strftime('%A')}",
        message=f"This is a reminder that you're in the Subcity Radio schedule for an episode of {slot.show.name} on {slot.start_time.strftime('%A %d/%m/%Y')} from {slot.start_time.strftime('%H:%M')}-{slot.end_time.strftime('%H:%M')}. If you can't make it, please let a member of our support team know ASAP. Otherwise, make sure to upload any custom comms, and we look forward to having you on the airwaves.\nYou've received this email because your show has reminders enabled in the Subcity Backend. If you don't want this, or think it's a mistake, get in contact with the support team and we'll be happy to help.",
        recipient_list=[u.email for u in slot.show.contributors.all()],
        from_email=None,
        fail_silently=True
    )

utilityTeamDayAdminObject = TeamDayAdmin() #it's weird how you have to do this in wagtail. why isn't this a static method?
numShowsToday = Slot.objects.filter(start_time__date = date.today()).filter(slot_type=Slot.EPISODE).count()

try:
    progDay = TeamDay.objects.get(team='p', day=date.today())
    send_mail(
        subject=f"Reminder: it's your Subcity programmes day",
        message=f"You're on programmes today! Thanks for helping out. There {'is 1 show today.' if numShowsToday == 1 else f'are {numShowsToday} shows today.'}",
        recipient_list=[progDay.team_member.email],
        from_email=None,
        fail_silently=True
    )
except:
    send_mail(
        subject=f"Nobody took today's programmes day!",
        message=f"It looks like there's no one down for a programmes day today in the backend. If no one has volunteered, we need to find someone to take today's programmes duties. If someone had volunteered, they've not been put in the backend. Go to https://subcity.org{utilityTeamDayAdminObject.url_helper.get_action_url('create')}?team=p&date={date.today().isoformat()} to input the correct info.",
        recipient_list=["programmes@subcity.org"],
        from_email=None,
        fail_silently=True
    )

try:
    commsDay = TeamDay.objects.get(team='c', day=date.today())
    send_mail(
        subject=f"Reminder: it's your Subcity comms day",
        message=f"You're on comms today! Thanks for helping out. Go to https://subcity.org/admin/comms for today's instagram story slides.",
        recipient_list=[commsDay.team_member.email],
        from_email=None,
        fail_silently=True
    )
except:
    send_mail(
        subject=f"Nobody took today's comms day!",
        message=f"It looks like there's no one down for a comms day today in the backend. If no one has volunteered, we need to find someone to take today's comms duties. If someone had volunteered, they've not been put in the backend. Make sure that somebody uploads the slides at https://subcity.org/admin/comms to the Instagram story today.",
        recipient_list=["comms@subcity.org"],
        from_email=None,
        fail_silently=True
    )