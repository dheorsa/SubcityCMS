from django.urls import path, reverse
from wagtail import hooks
from wagtail.admin.menu import MenuItem

from .views import index, keyslist


@hooks.register('register_admin_urls')
def register_support_url():
    return [
        path('support/keyslist/', keyslist, name='keyslist'),
        path('support/', index, name='support'),
    ]

@hooks.register('register_admin_menu_item')
def register_support_menu_item():
    return MenuItem('Support', reverse('support'), icon_name='group')