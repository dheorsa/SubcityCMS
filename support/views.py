from django.shortcuts import render
from user.models import User
from show.models import Show
from slot.models import Slot
from slot.admin import SlotAdmin
from django.contrib.auth.models import Group
from django.utils import dateformat
from datetime import datetime, timedelta, date, time
from django.core.exceptions import PermissionDenied

def getActiveContributorList():
    activeContributorList = []
    for show in Show.objects.filter(active=True):
        activeContributorList += show.contributors.all()
    return activeContributorList
    

def getEveningAccessList():
    eveningAccessList = []

    today = date.today()
    weekStart =  today - timedelta(days=today.weekday())

    for slot in Slot.objects.filter(slot_type=Slot.EPISODE).filter(start_time__gte=weekStart).filter(start_time__lte = weekStart + timedelta(days=7)).filter(start_time__time__gte=time(18,00)):
        eveningAccessList += slot.show.contributors.all()

    return eveningAccessList


def index(request):
    if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:
        slotTuples = []
        slotsNeedingSupport = Slot.objects.filter(needs_support = True).filter(start_time__gte=datetime.now()).all()
        utilitySlotAdminObject = SlotAdmin() #it's weird how you have to do this in wagtail. why isn't this a static method?
        for slot in slotsNeedingSupport:
            slotTuples.append((slot, utilitySlotAdminObject.url_helper.get_action_url('edit', slot.id)))

        return render(request, "support/index.html", {
            'allUsers': User.objects.all(),
            'allActiveContributors': set(getActiveContributorList()),
            'slotTuples' : slotTuples,
        })
    else:
        raise PermissionDenied()

def keyslist(request):
    if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:
        contributorList = list(set(getActiveContributorList()))
        teamList = User.objects.filter(groups__name='Team', is_active=True).exclude(username="admin").all()
        eveningAccessList = list(set(getEveningAccessList()))

        dayTimeAccessList = []
        for user in contributorList:
            if user.username != "admin" and user not in teamList and user not in eveningAccessList:
                dayTimeAccessList.append(user)

        dayTimeAccessList.sort(key= lambda user: user.first_name + " " + user.last_name)
        eveningAccessList.sort(key= lambda user: user.first_name + " " + user.last_name)

        return render(request, "support/keyslist.html", {
            'daytimeAccess':dayTimeAccessList,
            'eveningAccess':eveningAccessList,
            'team': teamList,
            'today': dateformat.DateFormat(datetime.now()).format("jS M, Y"),
            'today_numeric': datetime.now().strftime("%Y_%m_%d"),
            })
    else:
        raise PermissionDenied()