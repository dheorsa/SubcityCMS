import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "subcityCMS.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from slot.models import Slot
from show.models import Show
from datetime import date, timedelta, datetime

def doesItClash(newSlot):
    return Slot.objects.filter(end_time__gt=newSlot.start_time, start_time__lt=newSlot.end_time).exists()

DAYS_AHEAD_TO_GENERATE = 7 * 8 #generate 8 weeks in advance
today = date.today()

#django weekdays are sunday = 1 saturday = 7
#python isoweekdays are monday = 1 sunday = 7
#because nothing in life can be easy
shows = Show.objects.filter(auto_generate_slots=True, first_day_of_regular_slot__lte=today, first_day_of_regular_slot__week_day=[1,2,3,4,5,6,7,1][today.isoweekday()], )
day = today + timedelta(days=DAYS_AHEAD_TO_GENERATE)

for show in shows:
    if (day - show.first_day_of_regular_slot).days % (int(show.repeat_frequency) * 7) == 0:
        s = Slot(show=show, slot_type=Slot.EPISODE, start_time=datetime.combine(day, show.regular_start_time), end_time=datetime.combine(day, show.regular_end_time))
        if not doesItClash(s):
            s.save()

