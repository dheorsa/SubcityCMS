from django.urls import path, reverse
from wagtail import hooks
from wagtail.admin.menu import MenuItem

from .views import index


@hooks.register('register_admin_urls')
def register_stream_url():
    return [
        path('stream/', index, name='stream'),
    ]


@hooks.register('register_admin_menu_item')
def register_stream_menu_item():
    return MenuItem('Stream', reverse('stream'), icon_name='signal')

@hooks.register("register_icons")
def register_icons(icons):
    return icons + ['stream/signal.svg']