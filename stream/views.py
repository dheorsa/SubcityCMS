from django.shortcuts import render
from django.core.exceptions import PermissionDenied
import requests
import xmltodict
import os

def index(request):
    if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:

        auth = requests.auth.HTTPBasicAuth('admin', os.environ.get('ICECAST_ADMIN_PASSWORD', 'hackme'))
        server_data = xmltodict.parse(requests.get("http://subcity-icecast:8000/admin/stats", auth=auth).content, attr_prefix="")

        if isinstance(server_data["icestats"]["source"], dict): #if there's only one source, make it a list anyway so the template works
                                                                #this is because of the format of icecast stats.xml and how xmltodict parses it
            server_data["icestats"]["source"] = [server_data["icestats"]["source"]]

        return render(request, "stream/index.html", server_data)
    else:
        raise PermissionDenied