import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "subcityCMS.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from django.db.models import Q
from slot.models import Slot
import datetime
import b2sdk.v2 as b2
from subcityCMS.settings import B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY, B2_BUCKET_NAME

slots = Slot.objects.filter(end_time__lt=datetime.datetime.now() - datetime.timedelta(minutes=15),
                                end_time__gt=datetime.datetime.now() - datetime.timedelta(days=14),
                                slot_type=Slot.EPISODE).filter(Q(recording_url__isnull=True) | Q(recording_url=""))

print(slots)

if len(slots) > 0:
    info = b2.InMemoryAccountInfo()
    b2_api = b2.B2Api(info)

    b2_api.authorize_account("production", B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY)
    

    for s in slots:
        if s.show.slug:
            filename = s.show.slug + s.start_time.strftime("-%Y%m%d%H%M.mp3")
            try:
                b2_api.get_file_info_by_name(B2_BUCKET_NAME, filename)
                url = b2_api.get_download_url_for_file_name(B2_BUCKET_NAME, filename)
                print("found", url)
                s.recording_url = url
                s.save()
            except Exception:
                print("couldnae find", filename)
    