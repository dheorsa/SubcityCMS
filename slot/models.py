from django.db import models
from django.core.exceptions import ValidationError
from django.conf import settings
from django.utils import timezone
from django import forms
from wagtail.api import APIField
from wagtail.admin.panels import FieldPanel, FieldRowPanel, MultiFieldPanel
from show.models import Show
import datetime
import json

def doesItClash(newSlot):
    return Slot.objects.exclude(pk=newSlot.pk).filter(end_time__gt=newSlot.start_time, start_time__lt=newSlot.end_time).exists()

class Slot(models.Model):
    """Django model representation of a slot; for example a specific episode of a show, or a practice slot"""
    class Meta:
        permissions = [
            ("fully_edit_slot", "Can fully edit"),
        ]

    id = models.AutoField(primary_key=True)
    
    show = models.ForeignKey(Show, on_delete=models.PROTECT, blank=True, null=True)
    
    EPISODE = 'E'
    PRACTICE = 'P'
    MAINTENANCE = 'M'
    TYPE_CHOICES = [
        (EPISODE, 'Episode'),
        (PRACTICE, 'Practice'),
        (MAINTENANCE, 'Maintenance'),
    ]
    slot_type = models.CharField(max_length=1, choices=TYPE_CHOICES, default=EPISODE, blank=False, null=False)

    needs_support = models.BooleanField(blank=False, null=False, default=False, help_text="Does someone from the team need to sit in on this slot?")

    episode_title = models.CharField(blank=True, null=True, max_length=150)
    
    episode_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True, blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    episode_description = models.CharField(blank=True, null=True, max_length=2048)

    notes = models.CharField(blank=True, null=True, max_length=2048)

    attending = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, help_text="Who will be there?")

    start_time = models.DateTimeField(blank= False, null=False, auto_now=False, auto_now_add=False, default=timezone.now)
    end_time = models.DateTimeField(blank= False, null=False, auto_now=False, auto_now_add=False, default=timezone.now)

    recording_url = models.URLField(blank=True, null=True)

    publish_recording = models.BooleanField(default=False)

    recording_edit_lock = models.BooleanField(default=False)

    tracklist = models.JSONField(blank=True, null=True)

    def getLink(self):
        if self.slot_type == Slot.EPISODE:
            if self.show.slug:
                showid = self.show.slug
            else:
                showid = self.show.id
            return "/show/" + str(showid) + "/episode/" + str(list(Slot.objects.filter(show=self.show).order_by("start_time")).index(self) + 1)
        else:
            return None
        
    def tracklistAsDict(self):
        tracklist = None
        if self.tracklist:
            tracklist = json.loads(self.tracklist)
            for track in tracklist:
                if track["link"]:
                    if not track["link"].startswith("http"):
                        track["link"] = "http://" + track["link"]
        return tracklist


    # export fields over the api
    api_fields = [
        APIField('id'),
        APIField('show'),
        APIField('slot_type'),
        APIField('episode_title'),
        APIField('episode_image'),
        APIField('episode_description'),
        APIField('attending'),
        APIField('start_time'),
        APIField('end_time'),
    ]

    panels = [
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('slot_type', permission="slot.fully_edit_slot"),
                FieldPanel('needs_support', permission="slot.fully_edit_slot"),
            ]),
            FieldRowPanel([
                FieldPanel('start_time', permission="slot.fully_edit_slot"),
                FieldPanel('end_time', permission="slot.fully_edit_slot"),
            ]),
        ], heading="Slot Details"),
        MultiFieldPanel([
            FieldPanel('show', permission="slot.fully_edit_slot"),
            FieldPanel('episode_title'),
            FieldPanel('episode_image'),
            FieldPanel('episode_description', widget=forms.Textarea),
        ], heading="Show Details"),
        FieldPanel('attending', widget=forms.CheckboxSelectMultiple),
        FieldPanel('notes', widget=forms.Textarea),
    ]

    def clean(self):
        if self.slot_type == self.EPISODE and self.show == None:
            raise ValidationError(
                {'show': "Episodes need to specify which show they're an episode of"}
            )
        if self.end_time < self.start_time:
            raise ValidationError(
                {'end_time': "End time must come after start time"}
            )
        if self.end_time < (self.start_time + datetime.timedelta(minutes=15)):
            raise ValidationError(
                {'end_time': "Slots must be at least 15 minutes long"}
            )
        if doesItClash(self):
            raise ValidationError(
                "This slot clashes with a pre-existing slot, please check the schedule"
            )
        if self.needs_support == True and self.slot_type != self.EPISODE:
            raise ValidationError(
                "Please only mark episodes as needing support; if you want help during a practice session, ask someone directly :)"
            )

    def __str__(self):
        """String representation of this slot"""
        if self.slot_type == Slot.PRACTICE:
            return "Practice on " + self.start_time.strftime("%d/%m/%Y") + " from " + self.start_time.strftime("%H:%M") + "-" + self.end_time.strftime("%H:%M") 
        elif self.slot_type == Slot.MAINTENANCE:
            return "Maintenance on " + self.start_time.strftime("%d/%m/%Y") + " from " + self.start_time.strftime("%H:%M") + "-" + self.end_time.strftime("%H:%M") 
        elif self.episode_title:
            return self.show.name + ": " + self.episode_title + " on " + self.start_time.strftime("%d/%m/%Y") + " from " + self.start_time.strftime("%H:%M") + "-" + self.end_time.strftime("%H:%M")
        elif self.slot_type == Slot.EPISODE:
            return self.show.name + " episode on " + self.start_time.strftime("%d/%m/%Y") + " from " + self.start_time.strftime("%H:%M") + "-" + self.end_time.strftime("%H:%M")
        else:
            return "Slot on " + self.start_time.strftime("%d/%m/%Y") + " from " + self.start_time.strftime("%H:%M") + "-" + self.end_time.strftime("%H:%M")