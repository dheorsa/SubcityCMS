from wagtail_modeladmin.options import ModelAdmin, modeladmin_register
from wagtail_modeladmin.helpers import PermissionHelper
from django.contrib.admin import SimpleListFilter
from .models import Slot
from show.models import Show
from datetime import datetime
import time
from django.db.models.functions import Lower
from wagtail_modeladmin.mixins import ThumbnailMixin
from wagtail_modeladmin.views import DeleteView
from django.db.models import Q
from django.utils.translation import gettext as _
import b2sdk.v2 as b2
from subcityCMS.settings import B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY, B2_BUCKET_NAME
from urllib.parse import urlparse
import os

class SlotDeleteView(DeleteView):
    def confirmation_message(self):
        if self.instance.recording_url:
            return _(
                "Are you sure you want to delete this %(object)s? If other things in your "
                "site are related to it, they may also be affected. There is a recording "
                "attached to this slot, it will be PERMANENTLY DELETED."
            ) % {"object": self.verbose_name}
        else:
            return _(
                "Are you sure you want to delete this %(object)s? If other things in your "
                "site are related to it, they may also be affected. "
            ) % {"object": self.verbose_name}

    def delete_instance(self):
        if self.instance.recording_url:
            info = b2.InMemoryAccountInfo()
            b2_api = b2.B2Api(info)
            b2_api.authorize_account("production", B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY)
            bucket = b2_api.get_bucket_by_name(B2_BUCKET_NAME)

            filename = os.path.basename(urlparse(self.instance.recording_url).path)
            versions = bucket.list_file_versions(filename)
            for v in versions:
                bucket.delete_file_version(v.id_, filename)

        self.instance.delete()

    
class SlotPermissionHelper(PermissionHelper):
    def user_can_edit_obj(self, user, obj):
        """
        Return a boolean to indicate whether `user` is permitted to 'change'
        a specific `self.model` instance.
        """

        if (user.is_superuser or user.groups.filter(name='Team').exists() or user in obj.show.contributors.all()):
            perm_codename = self.get_perm_codename('change')
            return self.user_has_specific_permission(user, perm_codename)
            # The above two lines are just what the default PermissionHelper does
        else:
            return False
        

class DateFilter(SimpleListFilter):
    title = "date"
    parameter_name = "date"

    def lookups(self, request, model_admin):
        return [('past',"In the past"), ('future', "In the future")]

    def queryset(self, request, queryset):
        if self.value() == 'past':
            return queryset.filter(start_time__lte=datetime.now())
        elif self.value() == 'future':
            return queryset.filter(start_time__gte=datetime.now())

class ShowFilter(SimpleListFilter):
    title = "show"
    parameter_name = "show"

    def lookups(self, request, model_admin):
        shows = Show.objects.all().order_by(Lower('name'))
        return [(s.name, s.name) for s in shows]

    def queryset(self, request, queryset):
        if self.value():
            try:
                return queryset.filter(show__name=self.value())
            except (ValueError):
                return queryset.none()
            
class RecordingFilter(SimpleListFilter):
    title = "recording"
    parameter_name = "recording"

    def lookups(self, request, model_admin):
        return [('no_recording',"No recording"), ('unpublished', "Unpublished recording"), ('published', "Published recording")]
    
    def queryset(self, request, queryset):
        if self.value() == 'no_recording':
            return queryset.filter(Q(recording_url__isnull=True) | Q(recording_url__exact=''))
        elif self.value() == 'unpublished':
            return queryset.exclude(recording_url__isnull=True).exclude(recording_url__exact='').filter(publish_recording=False)
        elif self.value() == 'published':
            return queryset.exclude(recording_url__isnull=True).exclude(recording_url__exact='').filter(publish_recording=True)
                


class SlotAdmin(ThumbnailMixin, ModelAdmin):
    """Slot Admin"""

    def format_attending_for_list_view(self, obj):
        x = ""
        for u in obj.attending.all()[:]:
            x += (", " + str(u))
        return(x[2:])
    format_attending_for_list_view.short_description = 'attending'

    def format_duration_for_list_view(self, obj):
        return(time.strftime("%H:%M",time.gmtime((obj.end_time - obj.start_time).seconds))) #kind of ugly code here, just wanted to avoid the seconds column displaying as we don't really need it
    format_duration_for_list_view.short_description = 'duration'

    model = Slot
    menu_label = 'Slots'  # ditch this to use verbose_name_plural from model
    menu_icon = 'time'  # see the styleguide for more options
    menu_order = 1000  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('admin_thumb', 'show', 'slot_type', 'start_time', 'format_duration_for_list_view', 'episode_title', 'format_attending_for_list_view')
    list_display_add_buttons = 'show'
    search_fields = ('show__name', 'slot_type', 'start_time', 'end_time', 'episode_title')
    base_url_path = "slots"
    permission_helper_class = SlotPermissionHelper
    list_filter = ('slot_type', DateFilter, RecordingFilter, ShowFilter,)
    thumb_image_field_name = 'episode_image'
    delete_view_class = SlotDeleteView


    # Uncomment this to only display the slots a contributor owns to them in the list.
    # With it commented, they still can't edit other slots but they can read their info
    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     if request.user.groups.filter(name='Team').exists() or request.user.is_superuser:
    #         return qs
    #     elif request.user.groups.filter(name='Contributors').exists():
    #         return qs.filter(show__contributor=request.user)
    #     else:
    #         return None

modeladmin_register(SlotAdmin)
