# Generated by Django 4.2.13 on 2024-05-25 22:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('slot', '0014_slot_tracklist'),
    ]

    operations = [
        migrations.AddField(
            model_name='slot',
            name='recording_edit_lock',
            field=models.BooleanField(default=False),
        ),
    ]
