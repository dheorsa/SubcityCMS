# Generated by Django 3.2.19 on 2023-07-03 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('slot', '0005_slot_prerecord'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='slot',
            name='prerecord',
        ),
        migrations.AddField(
            model_name='slot',
            name='needs_support',
            field=models.BooleanField(default=False, help_text='Does someone from the team need to sit in on this slot?'),
        ),
    ]
