# Generated by Django 3.2.15 on 2022-10-25 18:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('slot', '0002_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='slot',
            name='epsiode_description',
            field=models.CharField(blank=True, max_length=2048, null=True),
        ),
    ]
