# /app/Dockerfile
# Pull the base image
FROM python:3.9-alpine3.20
# Add user that will be used in the container.
# RUN useradd wagtail

# Port used by this container to serve HTTP.
EXPOSE 8000

# Set environment variables.
# 1. Force Python stdout and stderr streams to be unbuffered.
# 2. Set PORT variable that is used by Gunicorn. This should match "EXPOSE"
#    command.
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1 \
    PORT=8000

# Set timezone
ENV TZ Europe/London

# Install server packages
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev libffi-dev openssl-dev \
    && apk add libheif-dev \
    && apk add jpeg-dev libwebp-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev libxml2-dev libxslt-dev libxml2 \
    && apk add tzdata \
    && apk add ffmpeg

# Install python packages
RUN pip install --upgrade pip
COPY requirements.txt / 
RUN pip install -r requirements.txt

# Postgres Entrypoint
COPY entrypoint.sh /
RUN chmod +x entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# Use /app folder as a directory where the source code is stored.
WORKDIR /app