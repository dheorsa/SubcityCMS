# Subcity CMS Backend

## Getting started

### Initial Docker + Linux Setup

- Download docker https://docs.docker.com/get-docker/
- If using docker on a windows machine I would HIGHLY recommend using WSL with Ubuntu or another Linux distro
- WSL + Ubuntu Installation: https://ubuntu.com/wsl

### Once you have Docker: 

- Navigate to the root folder SubcityCMS/

> docker compose up --build

In a web browser: *localhost:8000/admin*

If you then need a login for the admin (You will need to create a superuser each time you build the project)

> docker exec -it subcity-cms /bin/sh

Then run:

> python manage.py createsuperuser

That should be it!

## Development

### Resetting the Containers

Sometimes you may need to do a fresh install of the CMS during your dev work, for example when making (or pulling) significant changes to the data structure. This will delete the database, and under **NO CIRCUMSTANCES** should this command be run on the production server, unless you really really know what you're doing.

> docker compose down -v

### Applying changes

Once you've made an edit to the code, enter the shell with the command mentioned above

> docker exec -it subcity-cms /bin/sh

Then run the following

> python manage.py makemigrations

> python manage.py migrate

Not all changes require you to do this, if you haven't changed anything in a `models.py` file you're probably fine.

### Upgrading the server

**Make sure you back up the data before doing this on production!!! I'm not fucking around!!!**

First you have to take down the old containers
> docker compose -f docker-compose.prod.yml down

Then stash the secrets so they can be reloaded after pulling (since the git repository has dummy values)
> git stash

Pull the changes
>git pull

Pop the stash
>git stash pop

Run the production build process
> docker compose -f docker-compose.prod.yml up --build

You may need to enter the shell on the `subcity-cms` container by running 
> docker exec -it subcity-cms /bin/sh

Then run either

> python manage.py collectstatic

and/or

> python manage.py migrate

## Backups

### Backing up the backend data

First stop the containers
> docker compose -f docker-compose.prod.yml stop

Create a temporary container that mounts the database, as well as a bind mount to save the data to the host. Run the `tar cvf` command on the database directory and save it in the bind mount. Remember to change the date to today's date!
> docker run --rm --volumes-from subcity-cms-db -v ~/backup/24-01-2023:/backup ubuntu tar cvf /backup/dbdata.tar -C /var/lib/postgresql/data/ .

Do the same thing for the static files
> docker run --rm --volumes-from subcity-cms -v ~/backup/24-01-2023:/backup ubuntu tar cvf /backup/staticdata.tar -C /app/staticfiles/ .

And for the media files
> docker run --rm --volumes-from subcity-cms -v ~/backup/23-01-2023:/backup ubuntu tar cvf /backup/mediadata.tar -C /app/mediafiles .

Start the containers again
> docker compose -f docker-compose.prod.yml start

### Restoring from a backup
You'll need to change the volumes in `docker-compose.yml` to contain `external: true`

> docker run -v postgres_data:/var/lib/postgresql/data/ --name dbrestore ubuntu /bin/bash

> docker cp ~/backup/dbdata.tar dbrestore:/var/lib/postgresql/data/dbdata.tar

> docker run --rm --volumes-from dbrestore ubuntu bash -c "cd /var/lib/postgresql/data/ && tar xvf ./dbdata.tar"

> docker run --rm --volumes-from dbrestore ubuntu bash -c "rm /var/lib/postgresql/data/dbdata.tar"

> docker rm dbrestore

To replace the media and static files, the steps are similar.

## Wireguard

To get the config info for the local PC that has to tunnel to the server, run the following commands while ssh'd into the server

> sudo docker exec -it wireguard bash

> cat /config/peer1/peer1.conf
