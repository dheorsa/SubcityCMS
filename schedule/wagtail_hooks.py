from django.urls import path, reverse
from wagtail import hooks
from wagtail.admin.menu import MenuItem

from .views import index


@hooks.register('register_admin_urls')
def register_schedule_url():
    return [
        path('schedule/<urlDate>', index, name='schedule'),
        path('schedule/', index, name='schedule'),
    ]

@hooks.register('register_admin_menu_item')
def register_schedule_menu_item():
    return MenuItem('Schedule', reverse('schedule'), icon_name='date')