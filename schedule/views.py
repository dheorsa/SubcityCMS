from django.shortcuts import render
import datetime
from slot.models import Slot
from slot.admin import SlotAdmin
from teamDay.models import TeamDay
from teamDay.admin import TeamDayAdmin
from django.http import HttpResponse
import json
from wagtail.images.views.serve import generate_image_url
import re

def getWeekStart(urlDate=None):
    if (urlDate is None or urlDate == "null"):
        chosenDate = datetime.date.today()
    else:
        chosenDate = datetime.datetime.strptime(urlDate, "%Y%m%d").date() # yyyymmdd date format for urls
    return chosenDate - datetime.timedelta(days=chosenDate.weekday())

def getWeekOfSlots(weekStartDate, showsOnly):
    if showsOnly:
        return Slot.objects.filter(
            start_time__gte = weekStartDate,
            start_time__lte = weekStartDate + datetime.timedelta(days=7), #gonna need to be careful about edge case of a show that passes over the week bounday. come back to this
            slot_type = Slot.EPISODE
        ).order_by('start_time')
    else:
        return Slot.objects.filter(
            start_time__gte = weekStartDate,
            start_time__lte = weekStartDate + datetime.timedelta(days=7) #gonna need to be careful about edge case of a show that passes over the week bounday. come back to this
        ).order_by('start_time')

#slots is a list of every slot to be shown on the given week's schedule
#each slot is a dictionary
#label is the text to show on the schedule. for an episode, it should be the show name. for practice it should be the username
#day is which column to appear in. monday is 1, which is handy as the 0th column in the table is the time headings
#start is minutes into the day (for example, 1:35pm would be (13*60) + 35 = 815)
#duration is in minutes
#type is "episode" or "practice", which will determine the colour of the entry 
def index(request, urlDate=None):
    weekStartDate = getWeekStart(urlDate)

    utilityTeamDayAdminObject = TeamDayAdmin() #it's weird how you have to do this in wagtail. why isn't this a static method?

    progDayLinks = [None] * 7
    commsDayLinks = [None] * 7
    for i in range(7):
        try:
            progDayObject = TeamDay.objects.get(day = weekStartDate + datetime.timedelta(days=i), team = 'p')
            progDayLinks[i] = (str(progDayObject.team_member), utilityTeamDayAdminObject.url_helper.get_action_url('edit', progDayObject.id))
        except:
            progDayLinks[i] = ("Take day" ,utilityTeamDayAdminObject.url_helper.get_action_url('create') + "?team=p&date=" + (weekStartDate + datetime.timedelta(days=i)).isoformat())

        try:
            commsDayObject = TeamDay.objects.get(day = weekStartDate + datetime.timedelta(days=i), team = 'c')
            commsDayLinks[i] = (str(commsDayObject.team_member), utilityTeamDayAdminObject.url_helper.get_action_url('edit', commsDayObject.id))
        except:
            commsDayLinks[i] = ("Take day" ,utilityTeamDayAdminObject.url_helper.get_action_url('create') + "?team=c&date=" + (weekStartDate + datetime.timedelta(days=i)).isoformat())

    slotsThisWeek = getWeekOfSlots(weekStartDate, False)
    slotList = []
    for s in slotsThisWeek:
        if (s.slot_type == Slot.PRACTICE):
            entryType = "practice"
            entryLabel = ""
            for u in s.attending.all()[:]:
                entryLabel += (", " + str(u))
            entryLabel = entryLabel[2:]
        elif (s.needs_support):
            entryType = "needs_support"
            entryLabel = str(s.show)
        elif (s.slot_type == Slot.EPISODE):
            entryType = "episode"
            entryLabel = str(s.show)
        elif (s.slot_type == Slot.MAINTENANCE):
            entryType = "maintenance"
            entryLabel = "Maintenance"
        
        entryDay = s.start_time.weekday() + 1
        entryStart = (s.start_time.time().hour * 60) + s.start_time.time().minute
        entryDuration = int((s.end_time - s.start_time).total_seconds() // 60)
        utilitySlotAdminObject = SlotAdmin() #it's weird how you have to do this in wagtail. why isn't this a static method?
        entryURL = utilitySlotAdminObject.url_helper.get_action_url('edit', s.id)
        slotList.append({'label':entryLabel, 'day':entryDay, 'start':entryStart, 'duration':entryDuration, 'type':entryType, 'url':entryURL})

    return render(request, "schedule/index.html", 
    {
        'weekStartDate': weekStartDate,
        'tuesdayDate' : weekStartDate + datetime.timedelta(days=1),
        'wednesdayDate' : weekStartDate + datetime.timedelta(days=2),
        'thursdayDate' : weekStartDate + datetime.timedelta(days=3),
        'fridayDate' : weekStartDate + datetime.timedelta(days=4),
        'saturdayDate' : weekStartDate + datetime.timedelta(days=5),
        'sundayDate' : weekStartDate + datetime.timedelta(days=6),
        'lastWeek': weekStartDate - datetime.timedelta(days=7),
        'nextWeek': weekStartDate + datetime.timedelta(days=7),
        'slots':slotList,
        #2022/8/29 was the monday of a week 1. find number of weeks by floor dividing days delta by seven. modulo 4 to wrap around every 4 weeks. +1 to start from 1 rather than 0
        'weekNumber': ((((weekStartDate - datetime.date(2022,8,29)).days // 7) % 4) + 1),
        'progDayLinks': progDayLinks,
        'commsDayLinks': commsDayLinks,
    })

def frontendSchedule(request, urlDate=None):
    weekStartDate = getWeekStart(urlDate)
    slotsThisWeek = getWeekOfSlots(weekStartDate, True)
    scheduleDict = {'Monday':[], 'Tuesday':[], 'Wednesday':[], 'Thursday':[], 'Friday':[], 'Saturday':[], 'Sunday':[]}
    for s in slotsThisWeek:
        showName = str(s.show)
        entryStart = s.start_time.time().strftime("%H:%M")
        entryEnd = s.end_time.time().strftime("%H:%M")
        scheduleDict[s.start_time.strftime('%A')].append({'show':showName, 'start':entryStart, 'end':entryEnd})
    return HttpResponse(json.dumps(scheduleDict), content_type="application/json")

def nowPlaying(request):
    slotsNow = Slot.objects.filter(end_time__gt=datetime.datetime.now(), start_time__lte=datetime.datetime.now(), slot_type=Slot.EPISODE)
    if slotsNow:
        if slotsNow[0].episode_image:
            slotImage = generate_image_url(slotsNow[0].episode_image, 'fill-256x256')
        elif slotsNow[0].show.image:
            slotImage = generate_image_url(slotsNow[0].show.image, 'fill-256x256')
        else:
            slotImage = None
        return HttpResponse(json.dumps({'show':slotsNow[0].show.name, 'episode_name':slotsNow[0].episode_title, 'image':slotImage, 'timeout':60}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'show':None, 'episode_name':None, 'image':None, 'timeout':60}), content_type="application/json")

def showFromTimestamp(request, yyyy_mm_dd_hh_mm_ss=None):
    pattern = re.compile("\d{4}_[01]\d_[0-3]\d_[0-2]\d_[0-5]\d_[0-5]\d")
    if yyyy_mm_dd_hh_mm_ss == None:
        return HttpResponse(json.dumps({'response':"no datetime specified", 'success':False}))
    else:
        try:
            timestamp = datetime.datetime.strptime(yyyy_mm_dd_hh_mm_ss, '%Y_%m_%d_%H_%M_%S')
            slotsFound = Slot.objects.filter(end_time__gt=timestamp, start_time__lte=timestamp, slot_type=Slot.EPISODE)
            if slotsFound:
                return HttpResponse(json.dumps({'response':slotsFound[0].show.name, 'success':True}))
            else:
                return HttpResponse(json.dumps({'response':"no show at this time", 'success':False}))
        except:
            return HttpResponse(json.dumps({'response':"parsing datetime failed", 'success':False}))