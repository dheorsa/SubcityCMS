import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "subcityCMS.settings")

import sys

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from slot.models import Slot
from show.models import Show

import datetime
import b2sdk.v2 as b2
from subcityCMS.settings import B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY, B2_BUCKET_NAME


if len(sys.argv) == 2:
    splitArg = sys.argv[1].split("-")
    slug = splitArg[0]
    timestamp = splitArg[1].split(".")[0]

    if len(splitArg) == 2 and splitArg[1][-4:] == ".mp3" and len(splitArg[1]) == 16 and splitArg[1][:2] == "20" and splitArg[1][:-5].isnumeric():
        try:
            showObj = Show.objects.get(slug=slug)
            try:
                slotObj = Slot.objects.get(start_time=datetime.datetime.strptime(timestamp, "%Y%m%d%H%M"), show=showObj)
                
                info = b2.InMemoryAccountInfo()
                b2_api = b2.B2Api(info)
                b2_api.authorize_account("production", B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY)

                filename = sys.argv[1]
                try:
                    b2_api.get_file_info_by_name(B2_BUCKET_NAME, filename)
                    url = b2_api.get_download_url_for_file_name(B2_BUCKET_NAME, filename)
                    print("found", url)
                    slotObj.recording_url = url
                    slotObj.publish_recording = True
                    slotObj.save()
                    print("published")
                except Exception:
                    print("couldnae find", filename, "in backblaze")
                
            except Slot.DoesNotExist:
                print("can't find corresponding slot")
        except Show.DoesNotExist:
            print("can't find show with slug", splitArg[0])
    else:
        print("something wrong with filename")







