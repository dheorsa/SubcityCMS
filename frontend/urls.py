from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from show.feeds import ShowFeed

urlpatterns = [
    path("", views.index, name='index'),
    path("schedule/<urlDate>/", views.schedule, name='schedule'),
    path("schedule/", views.schedule, name='schedule'),
    path("chat/", views.chat, name="chat"),
    path("shows/", views.shows, name="shows"),
    path("show/<id>/", views.show, name="show"),
    path("show/<id>/feed/", ShowFeed()),
    path("show/<show_id>/episode/<episode_id>/", views.episode, name="episode"),
    path("login/", auth_views.LoginView.as_view(template_name="frontend/login.html")),
    path("dashboard/", views.dashboard, name="dashboard"),
    path("logout/", auth_views.LogoutView.as_view()),
    path("book-practice/", views.book_practice, name="book-practice"),
    path("cancel-practice/", views.cancel_practice, name="cancel-practice"),
    path("reset-password/", auth_views.PasswordResetView.as_view(template_name="frontend/reset-password.html", html_email_template_name="email/password_reset.html")),
    path("reset-password/<uidb64>/<token>", auth_views.PasswordResetConfirmView.as_view(template_name="frontend/reset-password-confirm.html"), name="password_reset_confirm"),
    path("reset-password/done/", auth_views.PasswordResetDoneView.as_view(template_name="frontend/reset-password-done.html"), name="password_reset_done"),
    path("reset-password/complete/", auth_views.PasswordResetDoneView.as_view(template_name="frontend/reset-password-complete.html"), name="password_reset_complete"),
    path("edit-recording/<id>/", views.edit_recording, name="edit-recording"),
    path("edit-episode-info/<id>/", views.edit_episode_info, name="edit-episode-info"),
    path("listen-back/", views.listen_back, name="listen-back"),
    path("rss-instructions/<id>/", views.rss_instructions, name="rss-instructions"),
    path("rss-instructions/", views.rss_instructions, name="rss-instructions"),
]