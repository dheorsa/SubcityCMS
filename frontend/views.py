from django.shortcuts import render, redirect
from schedule.views import getWeekStart, getWeekOfSlots
from datetime import timedelta, datetime, time
from show.models import Show
from wagtail.models import Page, Collection
from .models import HomePage
from django.db.models.functions import Lower
from wagtail.rich_text.pages import PageLinkHandler
from django.utils.html import escape
from django.http import Http404, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from slot.models import Slot
import tempfile
import requests
import subprocess
import os
from urllib.parse import urlparse
import b2sdk.v2 as b2
import json
from django.core.exceptions import ValidationError
from wagtail.images import get_image_model
from subcityCMS.settings import B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY, B2_BUCKET_NAME
from wagtail.log_actions import log
import threading

def process_recording(slotObject, startOffset, endOffset, fadeIn, fadeOut):
    if slotObject.recording_edit_lock == False:
        slotObject.recording_edit_lock = True
        slotObject.save()
        try:
            with tempfile.TemporaryDirectory() as td:
                print("td", td)
                with open(td + "/raw.mp3", "wb") as rawAudio:
                    response = requests.get(slotObject.recording_url)
                    if response.status_code == 200:
                        rawAudio.write(response.content)
                    else:
                        response.raise_for_status()
                
                    print("got file from b2")
                    rawDuration = int(float(subprocess.run(["ffprobe", "-v", "quiet", "-show_entries", "format=duration", "-of", "csv=p=0", td+"/raw.mp3"], capture_output=True, text=True).stdout.strip()))
                    print("rawDuration", rawDuration)
                    trimmedDuration = rawDuration - startOffset - endOffset

                    outputName = os.path.basename(urlparse(slotObject.recording_url).path)

                    if fadeIn > 0 or fadeOut > 0:
                        print("fading and trimming")
                        subprocess.run(["ffmpeg", "-i", td + "/raw.mp3", "-ss", str(timedelta(seconds=startOffset)), "-t", str(timedelta(seconds=trimmedDuration)), "-af", f"afade=in:st={startOffset}:d={fadeIn},afade=out:st={trimmedDuration+startOffset-fadeOut}:d={fadeOut}", "-b:a", "320k", td + "/" + outputName])
                    else:
                        print("just fading")
                        subprocess.run(["ffmpeg", "-ss", str(timedelta(seconds=startOffset)), "-t", str(timedelta(seconds=trimmedDuration)), "-i", td + "/raw.mp3", "-c", "copy", td + "/" + outputName])
                    print("file processed")

                    info = b2.InMemoryAccountInfo()
                    b2_api = b2.B2Api(info)

                    b2_api.authorize_account("production", B2_APPLICATION_KEY_ID, B2_APPLICATION_KEY)

                    bucket = b2_api.get_bucket_by_name(B2_BUCKET_NAME)
                    bucket.upload_local_file(local_file=td + "/" + outputName, file_name=outputName)
                    print("file uploaded")
                    slotObject.publish_recording = True
                    slotObject.save()
                    print("recording published")
                    slotObject.recording_edit_lock = False
                    slotObject.save()
        except:
            slotObject.recording_edit_lock = False
            slotObject.save()

@classmethod
def custom_expand_db_attributes(cls, attrs):
    try:
        page = cls.get_instance(attrs)
        return '<a onclick="navigate(event, \'{s}\')" href="{s}">'.format(s=escape(page.localized.specific.url))
    except Page.DoesNotExist:
        return "<a>"

PageLinkHandler.expand_db_attributes = custom_expand_db_attributes


def index(request):
    return render(request, "frontend/index.html",
    {
        'body' : getattr(HomePage.objects.first(), 'body', 'nothing here..'),
        'ticker_items' : getattr(HomePage.objects.first(), 'ticker_items', [])
    }
    )

def schedule(request, urlDate=None):
    weekStartDate = getWeekStart(urlDate)
    if request.user.groups.filter(name="Team").exists() or request.user.groups.filter(name="Contributors").exists():
        slotsThisWeek = getWeekOfSlots(weekStartDate, False)
    else:
        slotsThisWeek = getWeekOfSlots(weekStartDate, True)
    
    scheduleDict = {}

    for i in range(7):
        scheduleDict[weekStartDate + timedelta(days=i)] = []

    for s in slotsThisWeek:
        if s.slot_type == Slot.MAINTENANCE:
            showName = "Studio closed for maintenance"
        elif s.slot_type == Slot.PRACTICE:
            showName = "practice - " + ", ".join([u.first_name for u in s.attending.all()])
        elif s.episode_title:
            showName = str(s.show) + " - " + s.episode_title
        else:
            showName = str(s.show)
        entryStart = s.start_time.time().strftime("%H:%M")
        entryEnd = s.end_time.time().strftime("%H:%M")
        scheduleDict[s.start_time.date()].append((entryStart + " - " + entryEnd + " | " + showName, s.getLink()))

    return render(request, "frontend/schedule.html",
    {
        'scheduleDict' : scheduleDict,
        'previousWeek' : datetime.strftime(weekStartDate - timedelta(days=7), "%Y%m%d"),
        'nextWeek' : datetime.strftime(weekStartDate + timedelta(days=7), "%Y%m%d"),
        'ticker_items' : getattr(HomePage.objects.first(), 'ticker_items', [])
    })

def chat(request):
    return render(request, "frontend/chat.html", {
        'ticker_items' : getattr(HomePage.objects.first(), 'ticker_items', [])
    })

def shows(request):
    urlOrder = request.GET.get('o', 'r')#parameter is o, default value is r
    activeShowsOnly = request.GET.get('a', 't')#parameter is a, default value is t
    genreSearch = request.GET.get('g', '')

    showsData = Show.objects.all()

    if activeShowsOnly == "t":
        showsData = showsData.filter(active = True)

    if genreSearch:
        showsData = showsData.filter(genres__name__icontains=genreSearch)

    if urlOrder=="a":
        showsData = showsData.order_by(Lower('name'))
    else:
        urlOrder=="r"
        showsData = showsData.order_by('?')

    #this feels gross
    showsDataNoDuplicates = []
    for s in showsData:
        if s not in showsDataNoDuplicates:
            showsDataNoDuplicates.append(s)

    return render(request, "frontend/shows.html", {
        'shows' : showsDataNoDuplicates,
        'ticker_items' : getattr(HomePage.objects.first(), 'ticker_items', []),
        'url_order' : urlOrder,
        'activeShowsOnly' : activeShowsOnly,
        'genreSearch' : genreSearch,
    })

def show(request, id):
    if id is None:
        raise Http404("Show not found")
    if id.isnumeric():
        if Show.objects.filter(id=id).exists():
            showObject = Show.objects.get(id=id)
        else:
            raise Http404("Show not found")
    elif Show.objects.filter(slug=id).exists():
        showObject = Show.objects.get(slug=id)
    else:
        raise Http404("Show not found")
    
    return render(request, "frontend/show.html", {
            'show' : showObject,
            'listenBackSlots': Slot.objects.filter(publish_recording=True,show=showObject).exclude(recording_url__isnull=True).exclude(recording_url__exact='').order_by("-start_time")[:20]
        })
    
def episode(request, show_id, episode_id):
    if show_id is None or episode_id is None or episode_id.isnumeric() == False:
        raise Http404("Episode not found")
    if show_id.isnumeric():
        if Show.objects.filter(id=show_id).exists():
            showObject = Show.objects.get(id=show_id)
        else:
            raise Http404("Episode not found")
    elif Show.objects.filter(slug=show_id).exists():
        showObject = Show.objects.get(slug=show_id)
    else:
        raise Http404("Episode not found")
    
    try:
        slotObject = Slot.objects.filter(show=showObject).order_by("start_time")[int(episode_id)-1]
    except IndexError:
        raise Http404("Episode not found")

    show_edit_button = False
    if request.user.is_authenticated:
        if Show.objects.filter(id=showObject.id, contributors=request.user).exists() or request.user.groups.filter(name='Team').exists():
            show_edit_button = True
    

    return render(request, "frontend/episode.html", {
        'show' : showObject,
        'slot' : slotObject,
        'episode_number' : episode_id,
        'tracklist': slotObject.tracklistAsDict(),
        'show_edit_button': show_edit_button,
    })

def edit_episode_info(request, id):
    if id is None:
        raise Http404("Page not found")
    elif id.isnumeric():
        if Slot.objects.filter(id=id).exists():
            slotObject = Slot.objects.get(id=id)
        else:
            raise Http404("Page not found")
        
    if(Show.objects.filter(id=slotObject.show.id, contributors=request.user).exists() or request.user.groups.filter(name='Team').exists()) and slotObject.recording_edit_lock == False:
        if request.method == 'POST' and request.POST:
            if "episode_title" in request.POST and "episode_description" in request.POST and "tracklistJSON" in request.POST:
                slotObject.episode_title = request.POST["episode_title"]
                slotObject.episode_description = request.POST["episode_description"]
                tracklistJSON = request.POST["tracklistJSON"]
                print(tracklistJSON)

                #https://dev.to/lb/image-uploads-in-wagtail-forms-39pl
                if 'episode_image' in request.FILES:
                    ImageModel = get_image_model()
                    
                    try:
                        if request.FILES["episode_image"].size > 10 * 1024 * 1024 or not request.FILES["episode_image"].content_type in ["image/jpeg", "image/png", "image/gif"]: #10mb show image size limit
                            raise ValidationError
                        
                        new_episode_image = ImageModel(file=request.FILES["episode_image"], title=slotObject.show.name + " episode " + str(slotObject.start_time.strftime("%y-%m-%d")), collection=Collection.objects.get(name="Episode Images"))
                        new_episode_image.validate_constraints()
                        new_episode_image.clean()
                        new_episode_image.save()
                        slotObject.episode_image = new_episode_image
                    except:
                        return render(request, "frontend/edit-episode-info.html", {
                            'slot' : slotObject,
                            'show': slotObject.show,
                            'tracklist': slotObject.tracklistAsDict(),
                            'message': "There was an issue with the image you uploaded",
                        })

                if tracklistJSON:
                    try:
                        print(tracklistJSON)
                        json.loads(tracklistJSON)
                        slotObject.tracklist = tracklistJSON
                        slotObject.save()
                    except:
                        print("issue with tracklist json; skipping it")

                try:
                    slotObject.clean_fields()
                    slotObject.save()
                except ValidationError:
                    return render(request, "frontend/edit-episode-info.html", {
                        'slot' : slotObject,
                        'show': slotObject.show,
                        'tracklist': slotObject.tracklistAsDict(),
                        'message': "There was an issue with the details you provided",
                    })
                log(instance=slotObject, action="wagtail.edit", user=request.user)
                return redirect(slotObject.getLink())
            else:
                return HttpResponseBadRequest
        else:
            return render(request, "frontend/edit-episode-info.html", {
                'slot' : slotObject,
                'show': slotObject.show,
                'tracklist': slotObject.tracklistAsDict(),
            })
    else:
        return redirect(slotObject.getLink())

def dashboard(request):
    if (request.user.is_authenticated):
        users_shows = Show.objects.filter(contributors=request.user)
        
        return render(request, "frontend/dashboard.html", {
            'shows': users_shows,
            'upcoming_episodes': Slot.objects.filter(end_time__gt = datetime.now(), slot_type = Slot.EPISODE, show__in = users_shows),
            'upcoming_practice': Slot.objects.filter(start_time__gt = datetime.now(), slot_type = Slot.PRACTICE, attending = request.user),
            'pending_recordings': Slot.objects.filter(end_time__lt = datetime.now(), slot_type = Slot.EPISODE, show__in = users_shows).exclude(recording_url__isnull=True).exclude(recording_url__exact='').exclude(publish_recording=True),
        })
    else:
        return redirect('/login/')
    

def generateAvailablePracticeSlots():
    availablePracticeSlots = {}
    now = datetime.now()
    nextHour = datetime(year = now.year, month = now.month, day=now.day, hour=now.hour)

    for i in range(1, 7*24):
        s = nextHour + timedelta(hours=i)
        if s.time() >= time(8,00) and s.time() <= time(17,00):
            if not Slot.objects.filter(end_time__gt=s, start_time__lt=s + timedelta(hours=1)).exists():
                if s.strftime("%Y-%m-%d") in availablePracticeSlots:
                    availablePracticeSlots[s.strftime("%Y-%m-%d")] += [s.strftime("%H")]
                else:
                    availablePracticeSlots[s.strftime("%Y-%m-%d")] = [s.strftime("%H")]

    return availablePracticeSlots

@login_required
def book_practice(request):
    if request.method == 'POST' and request.POST:
        try:
            requestedStartTime = datetime.strptime(request.POST["date"] + "-" + request.POST["hour"], "%Y-%m-%d-%H")
            requestedEndTime = requestedStartTime + timedelta(hours=1)

            if Slot.objects.filter(end_time__gt=requestedStartTime, start_time__lt=requestedEndTime).exists():
                return render(request, "frontend/book-practice.html", {"slots": generateAvailablePracticeSlots(), "message":"Sorry, your selected slot clashes with another slot already in the schedule"})
            elif Slot.objects.filter(slot_type=Slot.PRACTICE, start_time__gt=datetime.now() - timedelta(hours=0.5*24*7), start_time__lt=datetime.now() + timedelta(hours=0.5*24*7), attending=request.user).count() >= 2:
                return render(request, "frontend/book-practice.html", {"slots": generateAvailablePracticeSlots(), "message":"Sorry, this booking would exceed the limit of 2 hours of practice per week. This is not calculated Monday-Friday, we use a rolling 7 day window."})
            else:
                newSlot = Slot(slot_type=Slot.PRACTICE, start_time=requestedStartTime, end_time=requestedEndTime)
                newSlot.save()
                newSlot.attending.set([request.user])
                newSlot.save()
                log(instance=newSlot, action="wagtail.create", user=request.user)
                return redirect('/dashboard/')
        except Exception as e:
            print("exception", e)
            return render(request, "frontend/book-practice.html", {"slots": generateAvailablePracticeSlots(), "message":"Something went wrong with your request"})
    else:
        if(Show.objects.filter(contributors=request.user).exists() or request.user.groups.filter(name='Team').exists()):
            return render(request, "frontend/book-practice.html", {"slots": generateAvailablePracticeSlots()})
        else:
            return redirect('/login/')
        
@login_required
def cancel_practice(request):
    if request.method == 'POST' and request.POST and request.POST['id']:
        if Slot.objects.filter(id=request.POST['id'], slot_type = Slot.PRACTICE, attending=request.user).exists():
            slotObject = Slot.objects.get(id=request.POST['id'], slot_type = Slot.PRACTICE, attending=request.user)
            log(instance=slotObject, action="wagtail.delete", user=request.user)
            slotObject.delete()
    
    return redirect('/dashboard/')

@login_required
def edit_recording(request, id):
    try:
        slotObject = Slot.objects.exclude(recording_url__isnull=True).exclude(recording_url__exact='').exclude(publish_recording=True).get(id=id)
    except:
        return redirect('/dashboard/')

    if(Show.objects.filter(id=slotObject.show.id, contributors=request.user).exists() or request.user.groups.filter(name='Team').exists()) and slotObject.recording_edit_lock == False:
        if request.method == 'POST' and request.POST:
            if (slotObject.recording_edit_lock):
                return redirect(slotObject.getLink())
            else:
                print(request.POST)
                if "tracklistJSON" in request.POST:
                    tracklistJSON = request.POST["tracklistJSON"]
                    try:
                        json.loads(tracklistJSON)
                        slotObject.tracklist = tracklistJSON
                        slotObject.save()
                    except:
                        print("issue with tracklist json; skipping it")

                if "startOffset" in request.POST and "endOffset" in request.POST and "fadeIn" in request.POST and "fadeOut" in request.POST and "tracklistJSON" in request.POST:
                    startOffset = int(request.POST["startOffset"])
                    endOffset = int(request.POST["endOffset"])
                    fadeIn = int(request.POST["fadeIn"])
                    fadeOut = int(request.POST["fadeOut"])

                    if (slotObject.end_time - slotObject.start_time).total_seconds() + (20*60) - startOffset - endOffset > 0 and 0 <= fadeIn <= 15 and 0 <= fadeOut <= 15:
                        pr_thread = threading.Thread(target = process_recording, args = (slotObject, startOffset, endOffset, fadeIn, fadeOut))
                        pr_thread.start()
                        return redirect(slotObject.getLink())

                    else:
                        print("something wrong with POST parameters")
                        slotObject.recording_edit_lock = False
                        slotObject.save()
                else:
                    print("something missing from POST")
                    slotObject.recording_edit_lock = False
                    slotObject.save()
                return redirect('/dashboard/')
        else:
            return render(request, "frontend/edit-recording.html", {"slot": slotObject,})
    else:
        return redirect(slotObject.getLink())
    
def listen_back(request):
    slots = Slot.objects.filter(publish_recording=True).exclude(recording_url__isnull=True).exclude(recording_url__exact='').order_by("-start_time")[:25]
    return render(request, "frontend/listen-back.html", {"slots": slots,})

def rss_instructions(request, id=None):
    if id is None:
        rss_link = None
        show_name = None
    elif id.isnumeric():
        if Show.objects.filter(id=id).exists():
            showObject = Show.objects.get(id=id)
            rss_link = "https://subcity.org" + showObject.getLink() + "/feed/"
            show_name = showObject.name
        else:
            raise Http404("Show not found")
    elif Show.objects.filter(slug=id).exists():
        showObject = Show.objects.get(slug=id)
        rss_link = "https://subcity.org" + showObject.getLink() + "/feed/"
        show_name = showObject.name
    else:
        rss_link = None
        show_name = None

    return render(request, "frontend/rss-instructions.html", {"rss_link": rss_link, "show_name": show_name})
    