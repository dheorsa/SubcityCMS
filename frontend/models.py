from django.db import models

from modelcluster.fields import ParentalKey
from wagtail.models import Page, Orderable
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.search import index

class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [InlinePanel("ticker_items", max_num=5, min_num=0, label="Ticker Item")],
            heading="News Ticker",
        ),
        MultiFieldPanel(
            [FieldPanel('body'),],
            heading="Homepage content",
        ),
    ]

    is_creatable = False
    max_count = 1
    subpage_types = ['frontend.ContentPage']


class TickerItem(Orderable):
    id = models.AutoField(primary_key=True)
    page = ParentalKey(HomePage, related_name="ticker_items")

    item_text = models.CharField(blank=False, null=False, max_length=64)

    panels = [
        FieldPanel("item_text")
    ]

class ContentPage(Page):

    # Database fields
    body = RichTextField()

    # Search index configuration
    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    # Editor panels configuration
    content_panels = Page.content_panels + [
        FieldPanel('body'),
    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        # Add extra variables and return the updated context
        context['ticker_items'] = HomePage.objects.all()[0].ticker_items
        return context


    # Parent page / subpage type rules
    parent_page_types = [HomePage, 'frontend.ContentPage'] #Main reason I'm mixing direct reference and wagtail string magic is a reminder to myself that I can do both
    subpage_types = ['frontend.ContentPage']