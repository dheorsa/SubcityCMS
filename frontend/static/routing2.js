function navigate(e, page){
    e.preventDefault();
    render(page);
    window.history.pushState({}, "", page);
}

function render(page){
    splitPath = page.split("/").filter(e => e);
    switch(splitPath[0]){
        case "chat":
            $('#content').load("/chat div#content");
            break;
        case "schedule":
            if (splitPath[1]){
                $('#content').load("/schedule/" + splitPath[1] + " div#content");
            }else{
                $('#content').load("/schedule div#content");
            }
            break;
        case "book-practice":
            $('#content').load("/book-practice div#content", function(){
                generateDayDropdown()
                updateHourDropdown()
            });
            break;
        default:
            try{
                $('#content').load(page + " div#content");
                break;
            }catch{
                $('#content').html('');
                break;
            }
            
    }
}

$(window).on('popstate', function(event) {
    render(window.location.pathname+window.location.search);
});
