const BLANKROW = "<td><input type=\"text\" style=\"width:100%;\" onchange=\"tracklistUpdated()\"></td>\
<td><input type=\"text\" style=\"width:100%;\" onchange=\"tracklistUpdated()\"></td>\
<td><input type=\"text\" maxlength=\"2\" size=\"2\" placeholder=\"00\" onchange=\"tracklistUpdated()\">:<input type=\"text\" maxlength=\"2\" size=\"2\" placeholder=\"00\" onchange=\"tracklistUpdated()\">:<input type=\"text\" maxlength=\"2\" size=\"2\" placeholder=\"00\" onchange=\"tracklistUpdated()\"></td>\
<td><input type=\"text\" style=\"width:100%;\" onchange=\"tracklistUpdated()\"></td>\
<td><button onclick=\"deleteRow(this)\">-</button> <button onclick=\"addRowBelow(this)\">+</button></td>"

// https://stackoverflow.com/questions/175739/how-can-i-check-if-a-string-is-a-valid-number
function isOneOrTwoDigits(str) {
    const regex = /^\d{1,2}$/;
    return regex.test(str);
}

function isTwoDigitsUnderSixty(str) {
    const regex = /^(?:[0-5][0-9])$/;
    return regex.test(str);
}

function formatTimeStamp(hour, minute, second){
    if (hour == "" && minute == "" && second == "") return ""

    if (!isOneOrTwoDigits(hour)) cleanHour = "00"
    else if (hour.length == 1) cleanHour = "0" + hour
    else cleanHour = hour

    if (minute == "") cleanMinute = "00"
    else if (minute.length == 1) cleanMinute = "0" + minute
    else cleanMinute = minute

    if (second == "") cleanSecond = "00"
    else if (second.length == 1) cleanSecond = "0" + second
    else cleanSecond = second

    if (isTwoDigitsUnderSixty(cleanMinute) && isTwoDigitsUnderSixty(cleanSecond)) return cleanHour + ":" + cleanMinute + ":" + cleanSecond
    else return ""
}

function addRowBelow(clickedButton){
    table = document.getElementById("tracklist-table")
    var newRow = table.insertRow(clickedButton.parentNode.parentNode.rowIndex + 1)
    newRow.innerHTML = BLANKROW
    tracklistUpdated()
}

function deleteRow(clickedButton){
    table = document.getElementById("tracklist-table")

    if (table.rows.length > 2){
        table.deleteRow(clickedButton.parentNode.parentNode.rowIndex)
    }
    tracklistUpdated()
}

function tracklistUpdated(){
    table = document.getElementById("tracklist-table")
    lastRow = table.rows[table.rows.length-1].cells
    let lastRowEmpty = 1

    for (let i = 0; i < lastRow.length; i++) {
        if (lastRow[i].firstChild.value != ""){
            lastRowEmpty = 0
        }
    }

    if (lastRowEmpty == 0){
        var newRow = table.insertRow(table.rows.length)
        newRow.innerHTML = BLANKROW
    }
}

function tracklistTableToJSON(){
    table = document.getElementById("tracklist-table")
    let tracks = []

    for (var i = 1, row; row = table.rows[i]; i++) {
        if (row.cells[0].firstChild.value != "" && row.cells[1].firstChild.value != ""){
            tracks.push({
                title:row.cells[0].firstChild.value,
                artist:row.cells[1].firstChild.value,
                timestamp:formatTimeStamp(row.cells[2].children[0].value, row.cells[2].children[1].value, row.cells[2].children[2].value),
                link:row.cells[3].firstChild.value,
            })
        }
    }
    return JSON.stringify(tracks)
}