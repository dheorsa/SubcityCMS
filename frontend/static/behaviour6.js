let nowPlayingInterval = setInterval(updateNowPlaying, 30000)
stopped = true

function pauseStream() {
    document.getElementById("playpause").src = imagesURL + "/play.svg";
    document.getElementById("bigplaypause").src = imagesURL + "/play.svg";
    
    unloadTimeout = setTimeout(() => { //just pausing the stream keeps buffering it and uses loads of bandwidth. if the user pauses for a minute uninterrupted, disconnect
        if (stream.paused){
            stopped = true;
            stream.src = null; //Null source to stop streaming
            stream.load();
        }
      }, 60000);
      
}

function playStream() {
    document.getElementById("playpause").src = imagesURL + "/pause.svg";
    document.getElementById("bigplaypause").src = imagesURL + "/pause.svg";
    stream.currentTime = stream.buffered.end(0)
    clearTimeout(unloadTimeout); // we need to reset the pause timer
}

function clickedPlay() {
    if(stream.paused){
        if (stopped){
            stream.src = "https://stream.subcity.org/listen";
            stream.load();
            stopped = false;
        }
        stream.play();
        playStream()
    }else{
        stream.pause();
        pauseStream()
    }
}

function changedVolume() {
    stream.volume = document.getElementById("volumecontrol").value;
}

function updateHourDropdown() {
    var selectedDate = document.getElementById('date').value

    document.getElementById('hour').innerHTML = ""

    if (selectedDate !== ""){
        var hours = JSON.parse(document.getElementById('slot-data').textContent)[selectedDate]

        hours.forEach(function(hour){
            var opt = document.createElement('option')
            opt.value = hour
            opt.innerHTML = hour + ":00"
            document.getElementById('hour').appendChild(opt)
        })
    }
}

function generateDayDropdown() {
    Object.keys(JSON.parse(document.getElementById('slot-data').textContent)).forEach(function(day) {
    var opt = document.createElement('option')
    opt.value = day
    opt.innerHTML = (new Date(day)).toDateString()
    document.getElementById('date').appendChild(opt)
})
}

$(document).ready(function() {
    stream = new Audio();
    stream.src = null;
    stream.onpause = pauseStream;
    stream.onplay = playStream;


    $("#morebutton").click(function(){
        $('#morelinks').slideToggle(speed="slow", callback=function(){
            $('#morebutton').html($('#morebutton').html() == '<li>more</li>' ? '<li>less</li>' : '<li>more</li>');
          });
    });

    var ticker = $('div.ticker');
    ticker.each(function() {
        var t = $(this),indent = t.width();
        t.ticker = function() {
            if (t.is(":visible")){
                indent--;
                t.css('text-indent',indent);
                if (indent < -1 * t.children('div.tickertext').width()) {
                    indent = t.width();
            }
            }
        };
        t.data('interval',setInterval(t.ticker,900/60));
    });
});

function updateNowPlaying(){
    $.getJSON('/api/nowplaying/', function(data) {
        if (data.show == null){
            $("#smallplayertext").html("off air")
            $("#bigplayertext").html("<h3>off air</h3>")
        } else {
            if (data.episode_name){
                $("#smallplayertext").html(["now playing: ", data.show, " - ", data.episode_name].join(''))
                $("#bigplayertext").html(["<h3>on air:<br>", data.show, " - ", data.episode_name, "</h3>"].join(''))
            }else{
                $("#smallplayertext").html(["now playing: ", data.show].join(''))
                $("#bigplayertext").html(["<h3>on air:<br>", data.show, "</h3>"].join(''))
            }
        }
        if (data.image == null){
            $("#show-image").attr("src",imagesURL + "/subcity_logo_square.jpg");
        } else {
            $("#show-image").attr("src",data.image);
        }
    });
}

window.addEventListener('load', function(){
    updateNowPlaying()
})

window.addEventListener('focus', function(){
    clearInterval(nowPlayingInterval)
    updateNowPlaying()
    nowPlayingInterval = setInterval(updateNowPlaying, 30000)
});