Number.prototype.mod = function (n) {
    "use strict";
    return ((this % n) + n) % n;
};

var c = document.getElementById("recording-preview-progress");
var ctx = c.getContext("2d");
ctx.fillStyle = "#ebee58"

var rawAudio = document.getElementById("raw-audio");
var previewPlayPause = document.getElementById("preview-play-pause");

let startOffset = 300 //5 * 60 seconds
let endOffset = -900 //15 * 60 seconds

let startHour = parseInt(document.getElementById("start-timestamp").innerHTML.split(":")[0], 10)
let startMinute = parseInt(document.getElementById("start-timestamp").innerHTML.split(":")[1], 10)
let startSecond = parseInt(document.getElementById("start-timestamp").innerHTML.split(":")[2], 10)
let endHour = parseInt(document.getElementById("end-timestamp").innerHTML.split(":")[0], 10)
let endMinute = parseInt(document.getElementById("end-timestamp").innerHTML.split(":")[1], 10)
let endSecond = parseInt(document.getElementById("end-timestamp").innerHTML.split(":")[2], 10)

rawAudio.onloadedmetadata = function(){ rawAudio.currentTime = startOffset;}

function refreshProgressBar(){
    ctx.fillStyle = "#d34038"
    ctx.fillRect(0,0,1000,10);
    ctx.fillStyle = "#ebee58"

    let trimmedDuration = rawAudio.duration - startOffset + endOffset
    let progress = (rawAudio.currentTime - startOffset) / trimmedDuration

    ctx.fillRect(0,0,(1000*progress),10)
}

rawAudio.addEventListener("timeupdate", function() {
    if (rawAudio.currentTime >= rawAudio.duration + endOffset){
        rawAudio.pause()
        previewPlayPause.innerText = "Play preview"
    } else if (rawAudio.currentTime < startOffset){
        rawAudio.currentTime = startOffset
    }

    if ((rawAudio.currentTime - startOffset) < document.getElementById("fade-in-length").value){
        rawAudio.volume = (rawAudio.currentTime - startOffset) / document.getElementById("fade-in-length").value
    }else if ((rawAudio.duration + endOffset - rawAudio.currentTime) < document.getElementById("fade-out-length").value){
        rawAudio.volume = (rawAudio.duration + endOffset - Math.floor(rawAudio.currentTime)) / document.getElementById("fade-out-length").value
    }else{
        rawAudio.volume = 1
    }

    refreshProgressBar()

    var playPositionTotalSeconds = ((startHour*60*60) + (startMinute*60) + startSecond + (Math.floor(rawAudio.currentTime) - startOffset)).mod(24*60*60)
    var playHour = Math.floor(playPositionTotalSeconds/(60*60))
    var playMinute = Math.floor((playPositionTotalSeconds - (playHour*60*60))/60)
    var playSecond = playPositionTotalSeconds - (playHour*60*60) - (playMinute*60)

    document.getElementById("play-position").innerHTML = String(playHour).padStart(2, '0') + ':' + String(playMinute).padStart(2, '0') + ':' + String(playSecond).padStart(2, '0')
})

function playPreview() {
    if (rawAudio.currentTime < rawAudio.duration + endOffset){
        if(rawAudio.paused){
            rawAudio.play();
            previewPlayPause.innerText = "Pause preview"
        }else{
            rawAudio.pause();
            previewPlayPause.innerText = "Play preview"
        }
    }
}

function jumpToStart(){
    rawAudio.currentTime = startOffset
}

function jumpToEnd(){
    rawAudio.currentTime = rawAudio.duration + endOffset
}

function skipBackTen(){
    if ((rawAudio.currentTime -10) >= startOffset){
        rawAudio.currentTime -= 10
    }else{
        rawAudio.currentTime = startOffset
    }
}

function skipForwardTen(){
    if ((rawAudio.currentTime +10) <= (rawAudio.duration + endOffset)){
        rawAudio.currentTime += 10
    }else{
        rawAudio.currentTime = (rawAudio.duration + endOffset)
    }
}

function adjustStart(t){
    if ((startOffset + t >= 0) && (startOffset + t) < (rawAudio.duration + endOffset)){
        startOffset += t

        var startTimeStampTotalSeconds = (startHour*60*60) + (startMinute*60) + startSecond
        startTimeStampTotalSeconds = (startTimeStampTotalSeconds + t).mod(24*60*60)
        startHour = Math.floor(startTimeStampTotalSeconds/(60*60))
        startMinute = Math.floor((startTimeStampTotalSeconds - (startHour*60*60))/60)
        startSecond = startTimeStampTotalSeconds - (startHour*60*60) - (startMinute*60)

        document.getElementById("start-timestamp").innerHTML = String(startHour).padStart(2, '0') + ':' + String(startMinute).padStart(2, '0') + ':' + String(startSecond).padStart(2, '0')
    }
}

function adjustEnd(t){
    if ((endOffset + t <= 0) && (endOffset + t + rawAudio.duration) > startOffset){
        endOffset += t

        var endTimeStampTotalSeconds = (endHour*60*60) + (endMinute*60) + endSecond
        endTimeStampTotalSeconds = (endTimeStampTotalSeconds + t).mod(24*60*60)
        endHour = Math.floor(endTimeStampTotalSeconds/(60*60))
        endMinute = Math.floor((endTimeStampTotalSeconds - (endHour*60*60))/60)
        endSecond = endTimeStampTotalSeconds - (endHour*60*60) - (endMinute*60)

        document.getElementById("end-timestamp").innerHTML = String(endHour).padStart(2, '0') + ':' + String(endMinute).padStart(2, '0') + ':' + String(endSecond).padStart(2, '0')
    }
}

function fadeUpdated(){
    if (document.getElementById("fade-in-length").value > 15){
        document.getElementById("fade-in-length").value = 15
    }
    if (document.getElementById("fade-out-length").value > 15){
        document.getElementById("fade-out-length").value = 15
    }

    if (document.getElementById("fade-in-length").value < 0){
        document.getElementById("fade-in-length").value = 0
    }
    if (document.getElementById("fade-out-length").value < 0){
        document.getElementById("fade-out-length").value = 0
    }
}

function publish(){
    const form = document.getElementById("django-form")
    document.getElementById("startOffset").value = startOffset
    document.getElementById("endOffset").value = Math.abs(endOffset)
    document.getElementById("fadeIn").value = document.getElementById("fade-in-length").value
    document.getElementById("fadeOut").value = document.getElementById("fade-out-length").value

    form.submit()
}